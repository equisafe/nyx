from typing import List
from dataclasses import dataclass
from pynyx.contracts import KYC, ContractParams, Issuer, IssuerParams, Token, TokenParams, Crowdsale, CrowdsaleParams, EventSink, ExchangeParams, Exchange, DvP, DvPParams
from pynyx.helpers import LOG, ZERO_ADDRESS


@dataclass
class EnvParams:
    EventSink: ContractParams = ContractParams()
    KYC: ContractParams = ContractParams()
    Issuer: IssuerParams = IssuerParams()
    Token: TokenParams = TokenParams()
    Crowdsale: CrowdsaleParams = CrowdsaleParams()
    Exchange: ExchangeParams = ExchangeParams()
    DvP: DvPParams = DvPParams()


class Env:
    def __init__(self, ligo, tezos, parent_env = None):
        self.ligo, self.tezos, self.parent_env = ligo, tezos, parent_env

    def setup_env(self, specs):
        return NotImplementedError

    def log_storage(self):
        return NotImplementedError


class EventSinkEnv(Env):
    def __init__(
            self,
            ligo,
            tezos,
            params: EnvParams = None,
            EventSinkContract: EventSink = EventSink,
        ):
        super().__init__(ligo, tezos)

        params = None if not params else params.EventSink
        self.event_sink = EventSink(ligo, tezos, params)


class KYCEnv(Env):
    def __init__(
            self,
            ligo,
            tezos,
            params: EnvParams = None,
            KYCContract: KYC = KYC,
        ):
        super().__init__(ligo, tezos)

        params = None if not params else params.KYC
        self.kyc = KYCContract(ligo, tezos, params)

    def setup_env(self, specs):
        add_kyc_member_params = KYC.make_add_members_param(specs)
        self.tezos.wait(self.kyc.ci.addMembers(add_kyc_member_params))

        return self

    def log_storage(self):
        LOG(self.kyc.ci.storage(), "KYC storage")
        return self


class IssuerEnv(Env):
    def __init__(
            self,
            ligo,
            tezos,
            params: EnvParams = None,
            IssuerContract: Issuer = Issuer,
            ParentEnv: KYCEnv = KYCEnv,
        ):
        parent_env = ParentEnv(ligo, tezos, params)

        self.kyc = parent_env.kyc

        super().__init__(ligo, tezos, parent_env)

        params = None if not params else params.Issuer
        self.issuer = IssuerContract(ligo, tezos, params)

    def setup_env(self, specs):
        self.parent_env.setup_env(specs)
        return self

    def log_storage(self):
        self.parent_env.log_storage()
        LOG(self.issuer.ci.storage(), "Issuer storage")
        return self


class TokenEnv(Env):
    def __init__(self,
                 ligo,
                 tezos,
                 params: EnvParams = None,
                 TokenContract: Token = Token,
                 ParentEnv: IssuerEnv = IssuerEnv,
                 ):
        parent_env = ParentEnv(ligo, tezos, params)

        if parent_env:
            self.kyc = parent_env.kyc
            self.issuer = parent_env.issuer

        super().__init__(ligo, tezos, parent_env)

        token_params = TokenParams(owner=tezos.addresses[0]) if not params else params.Token
        token_params.issuer = self.issuer.addr
        self.token = TokenContract(ligo, tezos, token_params)

    def setup_env(self, specs):
        self.parent_env.setup_env(specs)

        self.tezos.wait(self.issuer.ci.addToken(self.token.addr))
        self.tezos.wait(self.issuer.ci.setToken([self.token.addr, False]))

        return self

    def log_storage(self):
        self.parent_env.log_storage()
        LOG(self.token.ci.storage(), "Token storage")
        return self


class CrowdsaleEnv(Env):
    def __init__(
            self,
            ligo,
            tezos,
            params: EnvParams = None,
            CrowdsaleContract: Crowdsale = Crowdsale,
            ParentEnv: IssuerEnv = TokenEnv,
            ):
        parent_env = ParentEnv(ligo, tezos, params)

        if not parent_env.token.params.standalone:
            self.kyc = parent_env.kyc
            self.issuer = parent_env.issuer

        self.token = parent_env.token

        super().__init__(ligo, tezos, parent_env)

        default_params = CrowdsaleParams(
            owner=tezos.addresses[4],
            token=self.token.addr,
        )
        params = default_params if not params else params.Crowdsale
        params.token = self.token.addr
        self.crowdsale = Crowdsale(ligo, tezos, params)

    def setup_env(self, invs = []):
        self.parent_env.setup_env(invs)

        return self

    def log_storage(self):
        self.parent_env.log_storage()
        LOG(self.crowdsale.ci.storage(), "Crowdsale storage")
        return self


class ExchangeEnv(Env):
    def __init__(
            self,
            ligo,
            tezos,
            params: EnvParams = None,
            ExchangeContract: Exchange = Exchange,
            token_envs: List[TokenEnv] = [],
            ):
        self.token_envs = token_envs

        super().__init__(ligo, tezos)

        default_params = ExchangeParams(
            owner=tezos.addresses[4],
        )
        params = default_params if not params else params.Exchange
        self.exchange = Exchange(ligo, tezos, params)

    def setup_env(
            self,
            fiat_rates: List[int],
            tokens_maxs: List[int],
            tz_fiat_rate: int,
            tz_max: int
            ):
        # Add Tz
        self.exchange.set_currency(ZERO_ADDRESS, tz_fiat_rate, tz_max)

        # Add other currencies
        for i, token_env in enumerate(self.token_envs):
            currency = token_env.token.addr
            fiat_rate, tokens_max = fiat_rates[i], tokens_maxs[i]
            self.exchange.set_currency(currency, fiat_rate, tokens_max)
        return self

    def log_storage(self):
        for token_env in self.token_envs:
            token_env.log_storage()
        LOG(self.exchange.ci.storage(), "Exchange storage")
        return self


class DvPEnv(Env):
    def __init__(
            self,
            ligo,
            tezos,
            params: EnvParams = None,
            DvPContract: DvP = DvP,
            ):
        super().__init__(ligo, tezos)

        default_params = DvPParams()
        params = default_params if not params else params.DvP
        self.dvp = DvP(ligo, tezos, params)
