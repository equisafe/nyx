import pprint
from pytest_tezos.TestTypes import Map, Set, List

empty_set = lambda: Set(set([]), "int")
empty_map = lambda: Map({}, "int", "int")
empty_list = lambda: List([], "int")

pp = pprint.PrettyPrinter(indent=2)
def LOG(storage, msg=None):
    if msg:
        print("===", msg, "===\n")
    pp.pprint(storage)
    if msg:
        print("\n")


import pandas as pd

import os
import pynyx

csv_path = os.path.dirname(pynyx.__file__) + "/country-and-region-codes.csv"
country_codes = pd.read_csv(csv_path)
def get_id_for_region_code(region_code):
    bytes_col_name = country_codes.columns.tolist()[-1]
    res = region_code == country_codes['ISO 3166-2 Region Code']
    return country_codes[res][bytes_col_name].tolist()[0][2:]


ZERO_ADDRESS = "tz1burnburnburnburnburnburnburjAYjjX"


errors = {
    # KYC
    "11": "Only the owner of the contract modify KYC members",
    "12": "This investor is not a member.",
    "13": "Country restriction failed.",
    "14": "This investor is KYC restricted.",

    # Issuer
    "201": "Only owner can call this entrypoint",
    "202": "This investor is not registered with an unrestricted KYC registrar known to the issuer.",
    "203": "Investor is restricted at issuer level",
    "204": "You are not allowed to call this entrypoint.",
    "205": "This token is restricted at the issuer level.",
    "206": "Global investor limit exceeded.",
    "207": "Token does not exist.",
    "208": "Gloabl restriction activated.",
    "209": "Only an unrestricted KYC can call this entrypoint.",
    "210": "Restricted registrar",
    "211": "Country investor limit exceeded",
    "212": "Registrar contract does not exist",

    # Security token
    "31": "Only owner or issuer can call this method.",
    "32": "Emitter balance in not sufficient.",
    "33": "not enough tokens to burn",
    "34": "not enough tokens to mint",
    "35": "You are not allowed to allow transfer from for this investor",
    "36": "You are not allowed to call transfer from.",
    "37": "You cannot transfer this amount of tokens on behalf of this investor.",
    "38": "Previous allowance amount is different than the provided one.",

    # Crowdsale
    "41": "Crowdsale has not started yet.",
    "42": "Crowdsale is finished.",
    "43": "Crowdsale is completed.",
    "44": "Owner cannot participate in crowdsale.",
    "45": "Must pay a positive amount to contract.",
    "46": "The max amount of tokens has already been sold.",
    "47": "The max amount of fiat has already been reached.",
    "48": "Cannot exchange between same currencies.",

    # Event Sink
    "51": "Only the owner can set the permission.",
    "52": "You are not allowed to post an event.",

    # DvP
    "61": "This order does not exist.",
    "62": "Order deadline passed.",
    "63": "This entrypoint was called without sending the proper amount of Tz.",
    "64": "You are not allowed to call takeOrder for this investor",
}

def parse_contract_error(error_number):
    return errors[error_number]



