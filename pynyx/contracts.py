import requests

from dataclasses import dataclass
from datetime import datetime
from pytezos.michelson.pack import get_key_hash

from pytezos.client import PyTezosClient

from pytest_tezos.TestTypes import Address, Bool, Nat, String, Timestamp, List, Map

from pynyx.helpers import empty_map, empty_set, empty_list, ZERO_ADDRESS
from pynyx.storages import IssuingEntityStorage, KYCStorage, SecurityTokensStorage, CrowdsaleStorage, EventSinkStorage, CrowdsaleInfo, DvPStorage, MiniNyxStorage


class Timestamp(Timestamp):
    @staticmethod
    def time(d: datetime):
        d = d.__str__()
        time = d.split('.')[0] if '.' in d else d
        return Timestamp(time.replace(' ', 'T') + 'Z')

    def __repr__(self):
        return f"(\"{self.value}\" : {self.get_type()})"


@dataclass
class ContractParams:
    owner: str = ""


class Contract:
    SRC_PATH: str = ""

    def __init__(self, ligo, tezos, params: ContractParams = None):
        self.params = params if params else self._get_default_params(tezos)
        self.ligo, self.tezos = ligo, tezos
        self.ci, self.addr = self.deploy()

    def _get_default_params(self, tezos):
        return ContractParams(owner=tezos.addresses[0])

    def init_storage(self):
        return NotImplementedError

    def deploy(self):
        tx = self.ligo.compile(self.SRC_PATH).originate(
            self.tezos.client,
            self.init_storage(),
        )

        origination = tx.inject()
        contract_address = self.tezos.contract_address(origination)

        return self.tezos.client.contract(contract_address), contract_address

    def switch_key(self, client: PyTezosClient):
        method_list = list(self.ci.__dict__.keys())[4:]
        for method in method_list:
            fn = getattr(self.ci, method)
            fn.key = client.key

class EventSink(Contract):
    SRC_PATH = "contracts/Event_Sink.mligo"

    def init_storage(self):
        owner = Address(self.params.owner)
        return EventSinkStorage(owner, empty_set(), empty_map(), 0)


class KYC(Contract):
    SRC_PATH = "contracts/KYC_Registrar.mligo"

    def __init__(self, ligo, tezos, params: ContractParams = None):
        super().__init__(ligo, tezos, params)

    def init_storage(self):
        owner = Address(self.params.owner)
        return KYCStorage(owner, empty_map())

    @staticmethod
    def make_add_members_param(specs):
        add_members_params = []
        for s in specs:
            add_members_params.append(
                {
                    "address_0": s[0],
                    "country": s[1],
                    "expires": s[2],
                    "rating": s[3],
                    "region": s[4],
                    "restricted": s[5],
                }
            )
        return add_members_params


@dataclass
class IssuerParams(ContractParams):
    name: str = "Issuer"
    global_invest_limit: int = 0


class Issuer(Contract):
    SRC_PATH = "contracts/Issuing_Entity.mligo"

    def _get_default_params(self, tezos):
        return IssuerParams(owner=tezos.addresses[0])

    @staticmethod
    def make_set_account_param(spec):
        return {"address_0": spec[0], "registrar": spec[1], "restricted": spec[2]}


    @staticmethod
    def get_update_country_restrictions_params(
        country,
        country_invest_limit,
        min_rating,
        rating_restrictions,
    ):
        return {
            "bytes_0": country,
            "country_invest_limit": country_invest_limit,
            "min_rating": min_rating,
            "rating_restrictions": rating_restrictions,
        }

    def init_storage(self):
        return IssuingEntityStorage(
            String(self.params.name),
            Address(self.params.owner),
            empty_map(),
            empty_map(),
            Nat(self.params.global_invest_limit),
            empty_map(),
            Nat(0),
            empty_map(),
            empty_map(),
            empty_map(),
            Bool(False),
        )

    def set_country_limits(
            self,
            country,
            invest_limit,
            min_rating,
            rating_restrictions
        ):
        country_restriction = {
            "bytes_0": country,
            "country_invest_limit": invest_limit,
            "min_rating": min_rating,
            "rating_restrictions": rating_restrictions,
        }
        self.tezos.wait(self.ci.updateCountryRestrictions([country_restriction]))


@dataclass
class MiniNyxParams(ContractParams):
    name: str = "Token"
    tokens: int = 100
    symbol: str = "TK"
    issuer: str = ZERO_ADDRESS
    total_supply: bool = 0


class MiniNyx(Contract):
    SRC_PATH = "contracts/Mini_Nyx.mligo"

    def get_allowance_for(self, authority, transfer_for):
        val_expr = {"prim": "Pair", "args": [{"string": transfer_for}, {"string": authority}]}
        type_expr = {"prim": "pair", "args": [{"prim": "address"}, {"prim": "address"}]}
        key_hash = get_key_hash(val_expr, type_expr)
        big_map_id = self.ci.storage()['allow_transfer_from']
        url = f'http://tz:20000/chains/main/blocks/head/context/big_maps/{big_map_id}/' + key_hash
        return int(requests.get(url).json()['int'])

    def make_set_allow_transfer_from_params(self, authority, transfer_for, amount, remove=False):
        transfer_from_info = {
            'transfer_for': transfer_for,
            'amount': amount,
        }
        try:
            prev_amount = self.get_allowance_for(authority, transfer_for)
        except:
            prev_amount = 0
        return {
            'authority': authority,
            'transfer_from_info': transfer_from_info,
            'remove': remove,
            'prev_amount': prev_amount,
        }

    def _get_default_params(self, tezos):
        return MiniNyxParams(
            owner=tezos.addresses[0],
        )

    def init_storage(self):
        return MiniNyxStorage(
            String(self.params.name),
            empty_map(),
            Nat(self.params.tokens),
            empty_map(),
            String(self.params.symbol),
            Address(self.params.owner),
            Nat(self.params.total_supply),
        )


@dataclass
class TokenParams(ContractParams):
    name: str = "Token"
    tokens: int = 100
    symbol: str = "TK"
    issuer: str = ZERO_ADDRESS
    total_supply: bool = 0


class Token(Contract):
    SRC_PATH = "contracts/Security_Tokens.mligo"

    def get_allowance_for(self, authority, transfer_for):
        val_expr = {"prim": "Pair", "args": [{"string": transfer_for}, {"string": authority}]}
        type_expr = {"prim": "pair", "args": [{"prim": "address"}, {"prim": "address"}]}
        key_hash = get_key_hash(val_expr, type_expr)
        big_map_id = self.ci.storage()['allow_transfer_from']
        url = f'http://tz:20000/chains/main/blocks/head/context/big_maps/{big_map_id}/' + key_hash
        return int(requests.get(url).json()['int'])

    def make_set_allow_transfer_from_params(self, authority, transfer_for, amount, remove=False, prev_amount="unsafe", ci=None):
        transfer_from_info = {
            'transfer_for': transfer_for,
            'amount': amount,
        }
        try:
            prev_amount = self.get_allowance_for(authority, transfer_for)
        except:
            prev_amount = 0
        return {
            'authority': authority,
            'transfer_from_info': transfer_from_info,
            'remove': remove,
            'prev_amount': prev_amount,
        }

    def _get_default_params(self, tezos):
        return TokenParams(
            owner=tezos.addresses[0],
            issuer=tezos.addresses[0],
        )

    def init_storage(self):
        return SecurityTokensStorage(
            String(self.params.name),
            empty_set(),
            Address(self.params.issuer),
            Nat(self.params.tokens),
            Bool(False),
            empty_map(),
            String(self.params.symbol),
            Address(self.params.owner),
            Nat(self.params.total_supply),
        )

@dataclass
class CrowdsaleParams(ContractParams):
    token: str = ZERO_ADDRESS
    receiver: str = ZERO_ADDRESS
    crowdsale_start = datetime(2020, 1, 1)
    crowdsale_finish = datetime(2021, 1, 1)
    crowdsale_completed = datetime(1970, 1, 1)
    fiat_max = 100
    fiat = 0


class Crowdsale(Contract):
    SRC_PATH = "contracts/Crowd_Sale.mligo"

    def _get_default_params(self, tezos):
        return CrowdsaleParams(
            owner=tezos.addresses[0],
            token=tezos.addresses[0],
        )

    def init_storage(self):
        crowdsale_info = CrowdsaleInfo(
            start = self.params.crowdsale_start,
            finish = self.params.crowdsale_finish,
            completed = Bool(False),
            activated = Bool(True),
            token = Address(self.params.token),
            bonus_pct = List([], "nat"),
            bonus_times = List([], "nat"),
            )

        return CrowdsaleStorage(
            Address(self.params.owner),
            Address(self.params.receiver),
            empty_map(),
            Nat(self.params.fiat_max),
            Nat(self.params.fiat),
            crowdsale_info,
        )


@dataclass
class ExchangeParams(ContractParams):
    receiver: str = ZERO_ADDRESS
    fiat_max = 100
    fiat = 0


class Exchange(Contract):
    SRC_PATH = "contracts/Crowd_Sale.mligo"

    def set_currency(self, token_addr, fiat_rate, tokens_max):
        init_tokens = 0
        params = [token_addr, fiat_rate, tokens_max, init_tokens]
        self.tezos.wait(self.ci.setCurrency(*params))

    def _get_default_params(self, tezos):
        return ExchangeParams(
            owner=tezos.addresses[0],
            token=tezos.addresses[0],
        )

    def init_storage(self):
        crowdsale_info = CrowdsaleInfo(
            start = 0,
            finish = 0,
            completed = 0,
            activated = Bool(False),
            token = Address(ZERO_ADDRESS),
            bonus_pct = List([], "nat"),
            bonus_times = List([], "nat"),
            )

        return CrowdsaleStorage(
            Address(self.params.owner),
            Address(self.params.receiver),
            empty_map(),
            Nat(self.params.fiat_max),
            Nat(self.params.fiat),
            crowdsale_info,
        )

@dataclass
class DvPParams(ContractParams):
    pass

class DvP(Contract):
    SRC_PATH = "contracts/DvP.mligo"

    def _get_default_params(self, tezos):
        return DvPParams()

    def init_storage(self):
        return DvPStorage(
            Address(self.params.owner),
            empty_map(),
            counter=Nat(0)
        )
