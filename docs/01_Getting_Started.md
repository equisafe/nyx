# Introduction

This document details how to setup the development environment, where
you will need Ligo, Python, and a Tezos sandbox running. This will allow
you to run the tests for example. The first option consists in running
everything from Docker containers, it is the simplest of the two option.
The second shows how to install the various dependencies needed
manually.

# Docker

The simplest way to get started is to clone the project, build the
docker image, and start docker compose. You'll then be able to `docker
exec` into the container with Ligo and all the python dependencies
installed, and run Nyx against the sandbox in the other container:

``` bash
$ git clone git@gitlab.com:equisafe/nyx.git
$ cd nxy/
$ docker-compose up
```

Docker compose created two containers:

``` bash
$ docker ps
CONTAINER ID        IMAGE
8be94b1a3cfb        nyx:latest
4915f1ce915a        yourlabs/tezos
```

To run the tests, `docker exec` into the nyx container:

``` bash
# Hop into the nyx container
$ docker exec -it 8be94b1a3cfb /bin/bash

# Run the registrar tests
root@:/nyx# py.test tests/KYC_Registrar__tests.py 
================================================== test session starts ==================================================
platform linux -- Python 3.8.2, pytest-5.4.1, py-1.8.1, pluggy-0.13.1
rootdir: /nyx
plugins: tezos-0.1.3, ligo-0.1.3
collected 4 items                                                                                                       

tests/KYC_Registrar__tests.py ....                                                                                [100%]

================================================== 4 passed in 20.00s ===================================================

# You can see the sandbox blocks as follows:
root@c018087cb8f7:/nyx# curl http://tz:8732/chains/main/blocks/
[["BMRpzYfeXfQgGP24s5LQxGovLbVhYkL961d1LUWj4T3aoCfeuLR"]]
```

From there, you can modify the Nyx source code from your host machine
and run it against the sandbox by jumping into a container shell.

We can also access the sandbox container:

``` bash
$ docker exec -it 4915f1ce915a /bin/bash

# You can now use tezos-client
bash-5.0$ tezos-client rpc get /chains/main/blocks/head
...
{ "protocol": "PsCARTHAGazKbHtnKfLzQg3kms52kSRpgnDY982a9oYsSXRLQEb",
  "chain_id": "NetXjD3HPJJjmcd",
  "hash": "BL5nVaAa6i7Jj6yaTV8z9CCFYfRWVC9xaBX4xzeptWREnXD2ks1",
...
```

# Standalone dev

### Tezos sandbox

This section details how to setup the sandbox allowing to test the
contracts locally. We shall be using Yourlabs' docker image that
provides effortless access to the sandbox using port mapping between the
container and the host system:

``` bash
docker run -t -p 8732:8732 yourlabs/tezos  # add `-b` to run the container in "background" mode
```

We can easily verify that we can interact with the sandbox as follows:

``` bash
curl http://localhost:8732/chains/main/blocks/head/context/constants | jq .hard_gas_limit_per_operation
```

``` example
1040000
```

### Python

This documentation assumes that you have Python 3 available in your path
and that you have installed the project dependencies. For example:

``` python
pip install --user -r requirements.txt
```
