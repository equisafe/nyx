The Nyx protocol is a set of smart contracts developed in *Ligo* for the
[Tezos](https://tezos.com/) blockchain that allows for the Tokenization
of financial securities. It was developed with the configurability of
on-chain permissioning schemes in mind and is therefore sufficiently
flexible to be used in a wide range of jurisdictions, thus leveraging
the blockchain to maximize the transparency for all parties involved.

Indeed, many jurisdictions have restrictions on investors, which can become complex on certain financial securities. With the Nyx Security Token, it is possible to
fine-tune these restrictions to constrain a token transaction between
investors based on rating, country of origin and investor count among
others. Better yet, permissions can be *factored* between Security
Tokens thanks to the a modular design of the contracts. To understand
better how this is achieved, let us now introduce two terms:

  - an **entity** is a person or legal entity taking part in the
    protocol,
  - an **issuer** is an entity that *creates* and *sells* tokens.

In many jurisdictions, it is the *issuer* that needs to enforce rule on
its investors. Hence the Nyx protocol introduces an **Issuing Entity**
contract that defines mutable restrictions (based on investor country,
rating, and counts) responsible for allowing a transaction or not
between entities. *The Nyx Security Token is a simple ERC-20 like token
that calls into an issuer when transferring tokens to enforce these
restrictions*. The information related to investors, such as their
country of origin and rating are kept on a third contract, the **KYC
Registrar**, which can itself restrict any entities and hence *block* a
transfer independently of the issuer. This allows for great flexibility
as *multiple* KYC contracts and *multiple* security tokens can be
associated with an issuing entity, and a KYC contract can provide its
services to multiple issuing entities.

The following sequence diagram shows the interaction between the
contracts when transferring tokens between investors. Naturally, similar
checks are performed on the minting of new tokens:

``` mermaid
sequenceDiagram
    par transfer tokens
      rect rgba(0, 255, 0, .1)
        note right of Alice: Checks that Alice has sufficient balance
        Alice->>Token: transfer(Bob, n)
      end
      rect rgba(0, 255, 0, .2)
        note right of Token: Checks that the token is unrestricted and that investor<br/> limits, country, rating and global restrictions are met <br/> and updates investor counters
        Token->>Issuer: transferTokens(Alice, Bob, n)
      end
      rect rgba(0, 255, 0, .3)
        note right of Issuer: Checks investors are not KYC restricted
        Issuer->>KYC: checkMember([Alice, Bob])
      end
    end
```

If any of the checks performed by the various contracts fails, the
entire transaction fails. The error codes thrown by the various contract
(using Michelson's
[failwith](https://tezos.gitlab.io/whitedoc/michelson.html#control-structures)
instruction) can be found [here](../docs/98_Contract_Errors.md).

To summarize, the various checks performed when transferring tokens are
as follow:

1.  check that the debitor has enough tokens in his balance to perform
    the transfer,
2.  check that the token is not restricted at the issuer level,
3.  check that the KYC contract is not restricted at the issuer level,
4.  check that there is not a global lock on all tokens at the issuer
    level,
5.  the global investor count is still under the threshold after
    transferring tokens,
6.  for both the creditor and the debitor, check that:
      - the investor's country of origin is allowed at the issuer level,
      - the investor rating is greater or equal to the minimum rating
        required by the issuer,
      - the investor is not KYC restricted,
      - `post-carthage` branch **only**: check that the investor count
        by country and / or by rating is under the associated counter
        threshold.

When discovering the contract interfaces in the next chapters, we will
see how each of these restrictions can be set. We will first explore how
the [KYC Registrar](../docs/02_KYC_Registrar.md) contract works, then
introduce the [Issuing Entity](../docs/03_Issuing_Entity.md) and see how
it operates, before detailing how to use the [Security
Token](../docs/04_Security_Token.md) contract. The interfaces for each
of these contracts can be found
[here](../docs/97_Contract_Interfaces.md).

Because the flexibility offered by the Nyx Security Token comes at the
cost of relatively high gas usage per transfers, we also provide a [Mini
Nyx](../docs/05_Mini_Nyx.md) contract that is a *highly optimized*
ERC-20 like token that does not call the issuer nor the KYC contracts
when transferring tokens between investors. All of the gas costs per
transactions are summarized [here](../gas/combined.json).

Finally, the Nyx standard also provides a [Delivery vs.
Payement](../docs/06_DvP.md) contract that allows to exchange
*different* security tokens between investors. Unfortunately, with the
Carthage gas restrictions and cross contract call costs, only exchanging
a Nyx Security Token for a Mini-Nyx token is possible at the moment.
However, tests for this case however are provided in the `post-carthage`
branch on a custom [Tezos Sandbox](../docs/99_Tezos_Sandbox.md) to
demonstrate that it would be possible should the gas limits per
operation be increased in future Tezos protocol updates.
