![](images/DvP.png)

The DvP (Delivery-Versus-Payment) contract allows for exchanging assets on chain. For instance,
Alice can place an order through the DvP contract which Bob can
take. The diagram below describes the sequence of events for this to
happen. As you can see, both Alice and Bob give rights to the DvP
contract to exchange the relevant amounts on their behalf once an order
is taken:

``` mermaid
sequenceDiagram
    Alice->>TokenA: allow_transfer_from(DvP, n)
    Alice->>DvP: make_offer(TokenA, n, TokenB, m, deadline)
    Bob->>TokenB: allow_transfer_from(DvP, m)
    Bob->>DvP: take_offer(alice_offer_id)
    par parallel
      DvP->>TokenA: transfer_from(Alice, Bob, n)
      DvP->>TokenB: transfer_from(Bob, Alice, m)
    end
```

Note that `TokenA` and `TokenB` on the above diagram can each be either
a Nyx security token, or a Mini Nyx token, or any token that matches
these two contract's `transferFrom` entrypoints since it is the only one
needed by the contract. Note also, that there is *no automatic* matching
of orders, Bob chooses an order to respond to by using its *order ID*.

Let's get started by importing the DvP environment and override the
`setup_env` method. We'll deploy two standalone tokens and mint them for Alice
and Bob:

``` python
from pytest_ligo import Ligo
from pytest_tezos import Tezos
tezos = Tezos()

from pynyx.environments import DvPEnv, EnvParams, TokenEnv
from pynyx.contracts import TokenParams, ContractParams, IssuerParams, MiniNyx, MiniNyxParams

class Nyx(DvPEnv):
    def setup_env(self, alice, bob, bob_amount, alice_amount):
        def get_token():
            owner = self.tezos.addresses[0]
            token_params = MiniNyxParams(owner=owner)
            return MiniNyx(self.ligo, self.tezos, token_params)

        self.tokenA = get_token()
        self.tokenB = get_token()

        self.tezos.wait(self.tokenA.ci.mint({'amount': alice_amount, 'tr_to': alice}))
        self.tezos.wait(self.tokenB.ci.mint({'amount': bob_amount, 'tr_to': bob}))
```

Let's deploy the contracts:

``` python
alice, bob = [tezos.addresses[i] for i in range(2)]
alice_client, bob_client = [tezos.clients[i] for i in range(2)]
init_tokens = 100

nyx = Nyx(Ligo(), tezos)
nyx.setup_env(alice, bob, init_tokens, init_tokens)
```

The contract storage consists in a dictionary of order IDs to organize
information. Right now, the order book is empty:

``` python
nyx.dvp.ci.storage()
```

``` example
{'counter': 0, 'order_book': 213}
```

The DvP storage is composed of an order book, represented by a big map,
and an order counter. The order book maps the order number to its
associated information. The counter increases every order that is
placed. Since the counter is currently 0, the next order to be placed
will have key \`0\`, the next one, \`1\` *etc*. Hence the counter is an
upper bound on the big map keys, which is useful since retrieving big
map keys is not an easy problem. Moreover, as we will see, the order
info contains its associated big map key. This is because big maps store
the key hashes rather than the keys, hence when using a Tezos blockchain
visualizer that shows big map contents such as
<https://better-call.dev>, do not show the key (only their hash). Which
is problematic since, as we shall see, we use the big map keys
associated with an order info to take an order. By using a big map and
storing the key in each order info, it is easy to retrieve order numbers
to respond to an order. This allows the DvP contract to use a big map
rather than a map, and therefore store as many orders as needed without
consuming much gas.

Alternatively, if you are running the DvP contract from some backend,
you can keep track of the orders as you make and take them of chain
since each transactions will give you the big map difference, which you
can then show your users.

*Please open an issue if you have trouble retrieving the order numbers*.

## Exchanging between tokens

Let's place an order, we want to exchange 10 token A for 20 token B:

``` python
from datetime import datetime

order = {
    "amount_from": 10,
    "amount_to": 20,
    "deadline": datetime(2021, 5, 24).strftime("%Y-%m-%dT%H:%M:%SZ"),
    "sender": alice,
    "token_from": nyx.tokenA.addr,
    "token_to": nyx.tokenB.addr,
}
tezos.wait(nyx.dvp.ci.makeOrder(order))
```

As we can see, the order book now contains Alice's order assigned to
order ID, we know its ID is `0` since the counter is now `1`:

``` python
from pynyx.helpers import LOG
LOG(nyx.dvp.ci.storage(), "Current storage")
LOG(nyx.dvp.ci.big_map_get('order_book/' + str(0)), "Order number 0")
```

``` example
=== Current storage ===

{'counter': 1, 'order_book': 213}


=== Order number 0 ===

{ 'amount_from': 10,
  'amount_to': 20,
  'deadline': '2021-05-24T00:00:00Z',
  'nat_6': 0,
  'sender': 'tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx',
  'token_from': 'KT1QLYtN89G8azLWP7vnGYTCkheKYxQCrbKn',
  'token_to': 'KT1EBKoRF9N2wtnFoUSPhzzwVT1L1zTqTnus'}
```

Notice that the order now contains a key `nat_6`, containing the order
number associated with this number. This allows to store the non-hashed
key of this big map entry on chain.

Alice now gives the DvP contract the permission to transfer 10 of her
tokens if the order is matched:

``` python
alice_transfer_from_params = nyx.tokenA.make_set_allow_transfer_from_params(
    authority=nyx.dvp.addr,
    transfer_for=alice,
    amount=order["amount_from"],
)
nyx.tokenA.switch_key(alice_client)
nyx.tezos.wait(nyx.tokenA.ci.setAllowTransferFrom(alice_transfer_from_params))
```

Bob is interested in the offer and wants to grab it. He needs to give
the DvP contract the right to transfer 20 of his tokens to Alice:

``` python
bob_transfer_from_params = nyx.tokenA.make_set_allow_transfer_from_params(
    authority=nyx.dvp.addr,
    transfer_for=bob,
    amount=order["amount_to"],
)
nyx.tokenB.switch_key(bob_client)
nyx.tezos.wait(nyx.tokenB.ci.setAllowTransferFrom(bob_transfer_from_params))
```

Bob now calls the `takeOrder` entrypoint:

``` python
order_id = nyx.dvp.ci.storage()['counter'] - 1
nyx.dvp.switch_key(bob_client)
tezos.wait(nyx.dvp.ci.takeOrder(order_id))
```

The balances on both token contracts have been updated, and the order
removed from the DvP contract:

``` python
assert nyx.tokenA.ci.big_map_get('balances/' + alice) == init_tokens - order["amount_from"]
assert nyx.tokenA.ci.big_map_get('balances/' + bob) == order["amount_from"]
assert nyx.tokenB.ci.big_map_get('balances/' + alice) == order["amount_to"]
assert nyx.tokenB.ci.big_map_get('balances/' + bob) == init_tokens - order["amount_to"]
```

Notice that the big map counter did not decrease as the order was taken,
but that the order at big map ID `0` disappeared:

``` python
LOG(nyx.dvp.ci.storage()['counter'])
try:
    # The big_map_get call should throw since the order ID does not exist anymore
    nyx.dvp.ci.big_map_get('order_book/' + str(order_id))
    assert 0
except:
    pass
```

``` example
1
```

## Exchanging a token for Tz

We can also use the DvP contract to sell `n` tokens for `m` Tz. To do
so, when Alice places the order, she'll set the order `token_to` key to
`pynyx.helpers.ZERO_ADDRESS` to tell the contract she wants to exchange
her tokens for some Tz. If Bob is interested in Alice's offer, he will
have to call the `takeOffer` entrypoint sending the proper amount of Tz:

``` python
from pynyx.helpers import ZERO_ADDRESS

order = {
  "amount_from": 10,
  "amount_to": 20,
  "deadline": datetime(2021, 5, 24).strftime("%Y-%m-%dT%H:%M:%SZ"),
  "sender": alice,
  "token_from": nyx.tokenA.addr,
  "token_to": ZERO_ADDRESS,
}
resp = tezos.wait(nyx.dvp.ci.makeOrder(order))
order_id = nyx.dvp.ci.storage()['counter'] - 1
LOG(nyx.dvp.ci.big_map_get("order_book/" + str(order_id)))
```

``` example
{ 'amount_from': 10,
  'amount_to': 20,
  'deadline': '2021-05-24T00:00:00Z',
  'nat_6': 1,
  'sender': 'tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx',
  'token_from': 'KT1QLYtN89G8azLWP7vnGYTCkheKYxQCrbKn',
  'token_to': 'tz1burnburnburnburnburnburnburjAYjjX'}
```

Alice now needs to set her transfer from permissions so that the DvP can
transfer her tokens when her order is responded to by someone:

``` python
alice_transfer_from_params = nyx.tokenA.make_set_allow_transfer_from_params(
    authority=nyx.dvp.addr,
    transfer_for=alice,
    amount=order["amount_from"],
)
nyx.tokenA.switch_key(alice_client)
nyx.tezos.wait(nyx.tokenA.ci.setAllowTransferFrom(alice_transfer_from_params))
```

Bob can now respond to Alice's offer, since he is interested in
exchanging 20 Tz for 10 of Alice's tokens. He'll need to call the
entrypoint with 20 Tz for the contract not to fail:

``` python
from pytezos.rpc.errors import MichelsonRuntimeError
from pynyx.helpers import errors

nyx.dvp.switch_key(bob_client)
try:
  tezos.wait(nyx.dvp.ci.takeOrder(order_id))
except MichelsonRuntimeError as err:
    LOG(errors[eval(err.__str__())[0]["with"]["string"]])
```

``` example
'This entrypoint was called without sending the proper amount of Tz.'
```

Indeed, we need to call the `takeOrder` entrypoint as follows, we're
also saving Alice and Bob's balances with respect to Token A before we
execute the transaction:

``` python
bob_balance_before = nyx.tokenA.ci.big_map_get('balances/' + bob)
alice_balance_before = nyx.tokenA.ci.big_map_get('balances/' + alice)
```

``` python
from decimal import Decimal
resp = tezos.wait(nyx.dvp.ci.takeOrder(order_id).with_amount(Decimal(order['amount_to'])))
```

We can now check that the tokens were exchanged correctly:

``` python
# Check that bob got his tokens from alice
assert nyx.tokenA.ci.big_map_get('balances/' + alice) == alice_balance_before - order["amount_from"]
assert nyx.tokenA.ci.big_map_get('balances/' + bob) == bob_balance_before + order["amount_from"]
```

We also check that bob sent the proper amount to the dvp contract which
itself forwarded it to alice:

``` python
mutez_amount = str(int(order['amount_to'] * 1e6))

# Bob sent the Tz to the DvP contract
assert resp['contents'][0]['amount'] == mutez_amount
assert resp['contents'][0]['metadata']['operation_result']['balance_updates'][0]['contract'] == bob
assert resp['contents'][0]['metadata']['operation_result']['balance_updates'][0]['contract'] == bob
assert resp['contents'][0]['metadata']['operation_result']['balance_updates'][0]['change'] == '-' + mutez_amount
assert resp['contents'][0]['metadata']['operation_result']['balance_updates'][1]['contract'] == nyx.dvp.addr
assert resp['contents'][0]['metadata']['operation_result']['balance_updates'][1]['change'] == mutez_amount

# Alice got the Tz from the DvP contract
assert resp['contents'][0]['metadata']['internal_operation_results'][1]['amount'] == mutez_amount
assert resp['contents'][0]['metadata']['internal_operation_results'][1]['result']['balance_updates'][0]['contract'] == nyx.dvp.addr
assert resp['contents'][0]['metadata']['internal_operation_results'][1]['result']['balance_updates'][0]['change'] == '-' + mutez_amount
assert resp['contents'][0]['metadata']['internal_operation_results'][1]['result']['balance_updates'][1]['contract'] == alice
assert resp['contents'][0]['metadata']['internal_operation_results'][1]['result']['balance_updates'][1]['change'] == mutez_amount
```

## Exchange Tz for a token

Finally, we can also exchange `n` Tz for `m` specific tokens using the
DvP contract. This time, when placing the order, Alice will need to send
it with the same amount of Tz is indicated in the order key
`amount_from`, and set the order key `token_from` to
`pynyx.helpers.ZERO_ADDRESS`:

``` python
order = {
  "amount_from": 10,
  "amount_to": 20,
  "deadline": datetime(2021, 5, 24).strftime("%Y-%m-%dT%H:%M:%SZ"),
  "sender": alice,
  "token_from": ZERO_ADDRESS,
  "token_to": nyx.tokenB.addr,
}
try:
  tezos.wait(nyx.dvp.ci.makeOrder(order))
except MichelsonRuntimeError as err:
    LOG(errors[eval(err.__str__())[0]["with"]["string"]])
```

``` example
'This entrypoint was called without sending the proper amount of Tz.'
```

Hence, Alice should now call the entrypoint as follows:

``` python
tezos.wait(nyx.dvp.ci.makeOrder(order).with_amount(Decimal(order['amount_from'])))
order_id = nyx.dvp.ci.storage()['counter'] - 1
LOG(nyx.dvp.ci.big_map_get('order_book/' + str(order_id)))
```

``` example
{ 'amount_from': 10,
  'amount_to': 20,
  'deadline': '2021-05-24T00:00:00Z',
  'nat_6': 2,
  'sender': 'tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx',
  'token_from': 'tz1burnburnburnburnburnburnburjAYjjX',
  'token_to': 'KT1EBKoRF9N2wtnFoUSPhzzwVT1L1zTqTnus'}
```

Since Bob now wants to take Alice's offer, he'll need to set the
appropriate allowances for the DvP contract to be able to send Alice the
required tokens:

``` python
bob_transfer_from_params = nyx.tokenB.make_set_allow_transfer_from_params(
    authority=nyx.dvp.addr,
    transfer_for=bob,
    amount=order["amount_to"],
)
nyx.tokenB.switch_key(bob_client)
nyx.tezos.wait(nyx.tokenB.ci.setAllowTransferFrom(bob_transfer_from_params))
```

Bob can now taken Alice's order:

``` python
bob_balance_before = nyx.tokenB.ci.big_map_get('balances/' + bob)
alice_balance_before = nyx.tokenB.ci.big_map_get('balances/' + alice)
nyx.dvp.switch_key(bob_client)
resp = tezos.wait(nyx.dvp.ci.takeOrder(order_id))
```

We can now assert that Alice got her tokens from Bob and that he got his
Tz from her:

``` python
# Check that bob got his tokens from alice
assert nyx.tokenB.ci.big_map_get('balances/' + bob) == bob_balance_before - order["amount_to"]
assert nyx.tokenB.ci.big_map_get('balances/' + alice) == alice_balance_before + order["amount_to"]

# Check that the dvp contract sent the proper amount to bob
mutez_amount = str(int(order['amount_from'] * 1e6))
assert resp['contents'][0]['metadata']['internal_operation_results'][0]['amount'] == mutez_amount
assert resp['contents'][0]['metadata']['internal_operation_results'][0]['result']['balance_updates'][0]['contract'] == nyx.dvp.addr
assert resp['contents'][0]['metadata']['internal_operation_results'][0]['result']['balance_updates'][0]['change'] == '-' + mutez_amount
assert resp['contents'][0]['metadata']['internal_operation_results'][0]['result']['balance_updates'][1]['contract'] == bob
assert resp['contents'][0]['metadata']['internal_operation_results'][0]['result']['balance_updates'][1]['change'] == mutez_amount
```
