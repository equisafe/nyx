| Code | Thrown by  | Message                                                                                 |
| ---- | ---------- | --------------------------------------------------------------------------------------- |
| 11   | KYC        | Only the owner of the contract modify KYC members                                       |
| 12   | KYC        | This investor is not a member.                                                          |
| 13   | KYC        | Country restriction failed.                                                             |
| 14   | KYC        | This investor is KYC restricted.                                                        |
| 201  | Issuer     | Only owner can call this entrypoint                                                     |
| 202  | Issuer     | This investor is not registered with an unrestricted KYC registrar known to the issuer. |
| 203  | Issuer     | Investor is restricted at issuer level                                                  |
| 204  | Issuer     | You are not allowed to call this entrypoint.                                            |
| 205  | Issuer     | This token is restricted at the issuer level.                                           |
| 206  | Issuer     | Global investor limit exceeded.                                                         |
| 207  | Issuer     | Token does not exist.                                                                   |
| 208  | Issuer     | Gloabl restriction activated.                                                           |
| 209  | Issuer     | Only an unrestricted KYC can call this entrypoint.                                      |
| 210  | Issuer     | Restricted registrar                                                                    |
| 211  | Issuer     | Country investor limit exceeded                                                         |
| 212  | Issuer     | Registrar contract does not exist                                                       |
| 31   | Token      | Only owner or issuer can call this method.                                              |
| 32   | Token      | Emitter balance in not sufficient.                                                      |
| 33   | Token      | not enough tokens to burn                                                               |
| 34   | Token      | not enough tokens to mint                                                               |
| 35   | Token      | You are not allowed to allow transfer from for this investor                            |
| 36   | Token      | You are not allowed to call transfer from.                                              |
| 37   | Token      | You cannot transfer this amount of tokens on behalf of this investor.                   |
| 38   | Token      | Previous allowance amount is different than the provided one.                           |
| 41   | Crowdsale  | Crowdsale has not started yet.                                                          |
| 42   | Crowdsale  | Crowdsale is finished.                                                                  |
| 43   | Crowdsale  | Crowdsale is completed.                                                                 |
| 44   | Crowdsale  | Owner cannot participate in crowdsale.                                                  |
| 45   | Crowdsale  | Must pay a positive amount to contract.                                                 |
| 46   | Crowdsale  | The max amount of tokens has already been sold.                                         |
| 47   | Crowdsale  | The max amount of fiat has already been reached.                                        |
| 48   | Crowdsale  | Cannot exchange between same currencies.                                                |
| 51   | Event Sink | Only the owner can set the permission.                                                  |
| 52   | Event Sink | You are not allowed to post an event.                                                   |
| 61   | DvP        | This order does not exist.                                                              |
| 62   | DvP        | Order deadline passed.                                                                  |
| 63   | DvP        | This entrypoint was called without sending the proper amount of Tz.                     |
