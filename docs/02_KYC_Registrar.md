The KYC Registrar contract keeps track of which investors are "KYC
restricted" or not. This is a stand-alone contract, that is, it does not
require any other contracts to be used and rather, provides a way for
other contracts to verify the KYC restrictions for an investor.

![](images/kyc.png)

The simplest way to deploy this contract is to use the abstraction
implemented in the contract tests. We start by making some imports:

``` python
# Import Ligo and Tezos helper classes
from pytest_ligo import Ligo
from pytest_tezos import Tezos
tezos = Tezos()
```

We can now deploy the KYC contract with `environments.KYCEnv`:

``` python
# Import the KYC contract abstraction
from pynyx.contracts import ContractParams

owner = tezos.addresses[0]
kyc_params = ContractParams(owner)
```

``` python
from pynyx.environments import KYCEnv, EnvParams
env_params = EnvParams(KYC=kyc_params)
nyx = KYCEnv(Ligo(), tezos, env_params)
```

We can inspect the contract storage as follows:

``` python
nyx.kyc.ci.storage()
```

``` example
{'members': 2979, 'owner': 'tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx'}
```

Note that the members storage entry is shown as an *integer*
representing the *big map ID* of the key. We'll see shortly how to
access elements from big maps.

Or by pretty printing it, which will show to be useful when dealing with
larger storages:

``` python
nyx.log_storage()
```

``` example
=== KYC storage ===

{'members': 2979, 'owner': 'tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx'}


<pynyx.environments.KYCEnv object at 0x7fd67eafb340>
```

And add and update members using the `addMembers` entrypoint:

``` python
import time
from pynyx.helpers import get_id_for_region_code

COUNTRIES = {
   "FR": get_id_for_region_code("FR-PAC"),
   "EN": get_id_for_region_code("GB-ENG"),
}

RATINGS = {
   "INDIVIDUAL": 1,
   "INSTITUTIONAL": 2,
}

current_timestamp = int(time.time())

members = [
    {
        "address_0": tezos.addresses[0],
        "country": COUNTRIES['FR'],
        "expires": 1245,
        "rating": 10,
        "region": 1,
        "restricted": False,
    },
    {
        "address_0": tezos.addresses[1],
        "country": COUNTRIES['FR'],
        "expires": 1245,
        "rating": 1,
        "region": 1,
        "restricted": False,
    },
    {
        "address_0": tezos.addresses[2],
        "country": COUNTRIES['EN'],
        "expires": 12345,
        "rating": 1,
        "region": 2,
        "restricted": True,
    },
]

tezos.wait(nyx.kyc.ci.addMembers(members))
nyx.log_storage()
```

``` example
=== KYC storage ===

{'members': 2979, 'owner': 'tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx'}
```

We can now fetch the members information for `tezos.addresses[0]` with
PyTezos's `big_map_get` as follows:

``` python
nyx.kyc.ci.big_map_get(f"members/{tezos.addresses[0]}")
```

``` example
{'country': '504143', 'expires': '1970-01-01T00:20:45Z', 'rating': 10, 'region': 1, 'restricted': False}
```
