import json
def simplify_gas():
    prefix, suffix = "" , ".json"
    names = ["kyc", "issuer", "token", "dvp", "event_sink", "mini_nyx"]
    files = [prefix + name + suffix for name in names]
    data = []
    for file_name in files:
        with open(file_name) as f:
            data += json.load(f)

    cleaned_data = {}
    for entry in data:
        for k, v in entry.items():
            if k in cleaned_data.keys():
                cleaned_data[k] += v
            else:
                cleaned_data[k] = v

    result = {k: {} for k in cleaned_data.keys()}
    for contract_name, entrypoints in cleaned_data.items():
        for entrypoint_info in entrypoints:
            entrypoint_name = entrypoint_info[0]
            call_gas_cost = entrypoint_info[1]
            if entrypoint_name in result[contract_name].keys():
                result[contract_name][entrypoint_name] += [int(call_gas_cost)]
            else:
                result[contract_name][entrypoint_name] = [int(call_gas_cost)]

    for contract_name, entrypoints in result.items():
        for entrypoint_name, gas_costs in entrypoints.items():
            gas_costs = result[contract_name][entrypoint_name]
            avg = int(sum(gas_costs) / len(gas_costs))
            result[contract_name][entrypoint_name] = {
                "min": min(gas_costs),
                "avg": avg,
                "max": max(gas_costs),
            }

    return result


import json
with open('combined.json', 'w', encoding='utf-8') as f:
    json.dump(simplify_gas(), f, ensure_ascii=False, indent=4)
