# NyX Standards 

*Nyx is the primordial goddess of the personified night and mother of Destiny and Hope. She is the first divinity issued from the primordial chaos.*
I take its references in the order that comes from chaos, the Chiasm moover that took the ladder. Every innovation is misunderstood, as easily as we misunderstand someone. NyX is the begining of something new, something that will change our behaviors. We can't foresee it yet, neither we foresaw looking at a live videos on our phones 20 years ago. But its our Hope, the mission to do better, to cultivate our curiousness, to always bring more efficiency and auditability, control and privacy. 
Hopefully, this endavour will bring more accessibility and liquidity in financial markets at the benefit of the investors. They will allows them to have, for the first time, a tool to follow their participation in stocks, funds, bonds, art pieces... to track them and control them.  {...}
Let us proceed, onwards.

# Abstract / Description 

This document outlines the NyX Standards, a set of digital Lego for building financial instruments on the Tezos blockchain. It is initiated and maintained by the team of Equisafe, a French based Tech Company on a mission to demoratise investment.

The protocol aims to replicates the financial infrastructure as we know it but on a distributed ledger to bring large scale interoperability, automation and efficiency. 

In the following space, we'll outline our approach and vision. We invite everyone the contributed and share his ideas. 

# Introduction

# How it works 

# Motivation

# Requirements

# Contracts

## Fundamental types


```ml
type limit_t = nat
type rating_t = nat
type country_t = nat
type region_t = nat
type counter_t = nat
type id_t = bytes
type amount_t = nat
type restricted_t = bool
type agreement_procedure_t = bool
type name_t = string
type registrar_t = address
type token_t = address
type owner_t = address
type issuer_t = address
type investor_t = address
type epoch_time_t = timestamp
type custodian_t = address
type reg_key_t = nat
type set_t = bool
type investor_id_t = bytes
type authority_id_t = bytes
type hash_t = bytes
```

### Helpers

```ml
type require_t = (cond : bool) * (error_msg : string) -> unit
```

## Modular

Functionalities can be added to the token and issuing entity contracts. The mechanism is described in this section as a sole contract. Note that in terms of implementation, the modular functionality is not a stand-alone contract, but rather additions to storages and entrypoints of contracts that wish to become modular.

### Storage

```ml
type module_t = address
type entrypoint_t = string
type permission_t = bool
type active_t = bool

type hook_info_t = {
  tag_bools : bool list ;
  permitted : permission_t ;
  active    : active_t ;
  always    : bool ;
}

type hooks_t = (entrypoint_t , hook_info_t) map
type permissions_t = (entrypoint_t , permission_t) map

type module_info_t = {
  active      : active_t ;
  set         : bool ;
  hooks       : hooks_t;
  permissions : permissions_t;
}

type active_modules_t = module_t set
type module_data_t = (module_t , module_info_t) map

type modules_info_t = {
  active_modules : active_modules_t ;
  module_data    : module_data_t ;
}
```

### Entrypoints

```ml
type attach_module_arg_t = module_t
type detach_module_arg_t = module_t
```

## Multi sig

### Storage

```ml
type authority_t = {
  signatures          : (bytes , bool) map ;
  multi_sig_auth      : (bytes , address list) map;
  multi_sig_threshold : limit_t ;
  address_count       : counter_t ;
  approved_until      : epoch_time_t ;
  restricted          : restricted_t;
}

type multisig_storage_t = {
  id_map         : (investor_t , address_info_t) map ;
  authority_data : (authority_id_t , authority_t) map ;
  owner_id       : investor_id_t ;
}
```

## Registrar

### Address info

```ml
type address_info_t = {
  id         : id_t ;
  restricted : restricted_t ;
}
```

### Investor ID & Investor info


```ml
type investor_id_t = bytes
type investor_info_t = {
    country    : country_t ;
    region     : region_t ;
    rating     : rating_t ;
    expires    : epoch_time_t ;
    restricted : restricted_t ;
    authority  : authority_id_t ;
}
```

### Authority info


```ml
type authority_info_t = {
  multiSigAuth      : (bytes , address list) map;
  countries         : country_t list ;
  multiSigThreshold : nat ;
  addressCount      : nat ;
  restricted        : bool ;
}
```

### Storage

```ml
type registrar_storage_t = {
  owner          : id_t ;
  members        : ( id_t , investor_info_t ) map ;
  id_map         : ( address , address_info_t ) map
  investor_data  : ( investor_id_t , investor_info_t ) map ;
  authority_data : ( authority_id_t , authority_info_t ) map ;
}
```

### Entrypoints

#### Storage Mutation

We provide storage mutations that are defined as:

```ml
type function_name_arg_t = arg_type
(* Leads to entrypoint `functionName` *)
let function_name (p: function_name_arg_t) (s: registrar_storage_t) : function_name_ret_t = ...
```

In cases where `function_name_ret_t` is not explicitely defined in the document, as per the above example, it is assumed to be defined as `type function_name_ret_t = (operation list * registrar_storage_t)` for mutations defined in this section.

We define bellow the public storage mutation for the contract by their argument following the above convention. As an example, the `add_members_t` argument presented in the snippet bellow correspond to the contract entrypoint `addMembers`. This entrypoint should be called with an argument of type `add_members_t` that can be obtained by defining in Ligo and compiling to Michelson.

```ml
(* The addMembers entrypoint also works for updating *)
type add_member_arg_t = ( investor_t * investor_info_t ) list
type remove_members_arg_t = investor_t list
type check_member_arg_t = investor_t
type register_addresses_arg_t = investor_t * (address list)
type restrict_addresses_arg_t = investor_t * (address list)
```

### Private functions

#### Getters

```ml
get_id_t : investor_t -> id_t
get_investor_t : investor_t -> investor_info_t
get_rating_t : id_t -> rating_t
get_region_t : id_t -> region_t
get_country_t : id_t -> country_t
get_expires_t : id_t -> expires_t
```

#### Others

```ml
is_premitted_t : address -> bool
is_premitted_id_t : id_t -> bool
authority_check_t : country_t * registrar_storage_t -> unit
generate_id_t : string -> bytes
add_addresses_t : ms_id_t * addresses_t -> id_map_t * nat
check_sender_is_owner_t : address_t -> unit
```


## Issuing entity

### Storage

```ml
type country_info_t = {
  counts       : nat list ;
  limits       : limit_t list ;
  permitted    : bool ;
  min_rating   : rating_t ;
}

type account_info_t = {
  count      : counter_t ;
  rating     : rating_t ;
  reg_key    : reg_key_t ;
  set        : set_t ;
  restricted : restricted_t ;
  custodian  : custodian_t ;
}
    
type token_info_t = {
  set        : set_t ;
  restricted : restricted_t ;
}

type document_info_t = {
  hash      : hash_t ;
  timestamp : epoch_time_t ;
}

type registrar_info_t = {
  kyc_registrar : registrar_t ;
  restricted    : restricted_t ;
}

type issuing_entity_storage_t = {
  name            : name_t ;
  owner           : owner_t ;

  locked          : bool ;
  kyc_registrars  : registrar_info_t set ;
  counts          : nat list ;
  limits          : limit_t list ;
  countries       : ( country_t , country_info_t ) map ;
  accounts        : (investor_id_t , account_info_t) map ;
  security_tokens : (token_t , token_info_t) map ;
  documents       : (string , document_info) map;
}
```

### Entrypoints

#### Storage mutation

```ml
type update_global_limit_arg_t = limit_t
type update_country_restrictions_arg_t = (country_t * country_restriction_t) list
type set_entity_restriction_arg_t = account_id_t * restricted_t
type set_token_restriction_arg_t = token_t * restricted_t
type add_token_arg_t = token_arg_t
type remove_token_arg_t = token_arg_t
type set_registrars_arg_t = registrar_t * restricted_t
type is_registered_investor_arg_t = investor_t
type set_document_hash_arg_t = document_id_t * document_hash_t
type remove_document_arg_t = document_id_t
```

### Private functions

#### Getters

```ml
get_document_hash : document_id_t -> hash_t
get_document_timestamp : document_id_t -> epoch_time_t
get_all_documents : issuing_entity_storage_t -> hash_t list
```

#### Others

```ml
fail_if_sender_not_owner : issuing_entity_storage_t) -> unit
```

## Security Token


### Approvals

```ml
type approval_t = {
  ap_from  : investor_t | issuer_t ; (* approval from  *)
  ap_to    : investor_t ;
  expires  : epoch_time_t ;
  tokens   : amount_t ; (*amount allowed*)
  executed : executed_t ;
}
```

### Storage

```ml
type security_token_storage_t = {
  module_info         : module_info_t ;

  name                : name_t ;
  symbol              : string ;
  decimals            : nat ;
  owner_id            : id_t ;
  issuer              : issuer_t ;
  agreement_procedure : agreement_procedure_t ;
  balances            : ( investor_t , amount_t ) map ;
  total_supply        : amount_t ;
  authorized_supply   : amount_t ;
  
  cust_balances       : ( address , ( address , nat ) map ) map ;
  allowed             : ( address , ( address , nat ) map ) map ;
  
  modules_info        : modules_info_t ;
}
```

### Entrypoints

#### Storage mutation

```ml
type add_approval_arg_t = approval_t
type transfer_arg_t = {
  tr_from : investor_t ;
  tr_to   : investor_t ;
  amount  : amount_t ;
}
type burn_arg_t = {
  tr_to  : investor_t ;
  amount : amount_t ;
}
type mint_arg_t = {
  tr_to  : investor_t ;
  amount : amount_t ;
}

type set_authorized_supply_arg_t = amount_t

type check_transfer_arg_t = {
  auth_id   : id_t ;
  id        : id_t ;
  custodian : custodian_t ;
  addresses : address list ;
  rating    : rating list ;
  country   : country list ;
  value     : amount_t ;
}

type transfer_from_arg_t = {
  from  : address ;
  to    : address ;
  value : amount_t ;
}

type allowance_arg_t = {
  owner   : owner_t;
  spender : investor_t ;
}

type custodian_balance_of_arg_t = {
  owner     : owner_t ;
  custodian : custodian_t ;
}

type check_transfer_custodian_arg_t = {
  custodian : custodian_t ;
  from      : investor_t ;
  to        : investor_t ;
  value     : amount ;
}

type transfer_custodian_arg_t = {
  sender   : address ;
  receiver : address :
  value    : amount_t ;
}
```

### Private functions

#### Getters

```ml
balance_of : investor_t -> amount_t
get_total_supply : security_token_storage_t -> amount_t
get_authorized_supply : security_token_storage_t -> amount_t
```

#### Private

```ml
check_approval_list : transfer_t -> approval_t list -> approval_t list option
make_transfer : transfer_t -> investor_amount_t -> investor_amount_t
check_permitted : security_token_storage_t -> unit
```

## Custodian

### Storage

This contract has an empty storage. Internally, it works by querying a Security Token contract, and hence indirectly uses its storage.

### Entrypoints

```ml
type balance_of_arg_t = (token_t * investor_t) * (nat -> operation list)
type check_custodian_transfer_arg_t = token_t * investor_t * investor_t * amount_t
type transfer_arg_t = token_t * investor_t * investor_t
type recieve_transfer_arg_t = investor_t * amount_t
type transfer_internal_arg_t = token_t * investor_t * investor_t * amount_t
```

## Security token modules

Modules available share a common core that we introduce before detailing the modules APIs.

### Module core

#### Storage

```ml
type st_module_core_storage_t = {
  owner_id : id_t ;
  owner : owner_t ;
  security_token : token_t ;
  issuer : issuer_t ;
}
```


### Dividend

#### Storage

```ml
type beneficiary = investor_t

type dividend_storage_t = {
  name            : string ;
  dividend_amount : amount_t ;
  clainExpiration : epoch_time_t ;
  claimed         : ( address , bool ) map ;
  claimed         : ( address , ( address , bool ) map ) map ;
  module_info     : st_module_core_storage_t ;
}
```

#### Entrypoints

```ml
type issue_dividend_arg_t = epoch_time_t
type claim_dividend_arg_t = beneficiary_t
type claim_many_arg_t = beneficiary_t list
type claim_custodian_dividend_arg_t = beneficiary_t * custodian_t
type claim_many_custodian_arg_t = beneficiary_t list * custodian_t
type close_dividend_arg_t = ()
```

### Vested Options

#### Storage

```ml
type option_info_t = {
  amount         : amount_t ;
  exercise_price : amount_t ;
  creation_date  : epoch_time_t ;
  vest_date      : epoch_time_t ;
}

type peg_t = amount
type option_id_t = id_t
type dividend_storage_t = {
  total_options            : amount_t ;
  tz_peg                   : peg_t ;
  expiry_date              : epoch_time_t ;
  termination_grace_period : epoch_time_t ;
  receiver                 : investor_t ;
  option_data              : ( option_id_t , option_info_t list) ;
  options                  : ( option_id_t , amount_t ) map ;
  module_info              : st_module_core_storage_t ;
}
```

#### Entrypoints

We first define some new types before introducing the storage:

```ml
type exercise_price_t = nat
type vest_date_t = epoch_time_t
```

```ml
type modify_peg_arg_t = peg_t
type issue_options_arg_t = id_t * amount_t * exercise_price_t * vest_date_t
type accelerate_vesting_date_arg_t = investor_id_t * ( option_id_t list ) * vest_date_t
type exercise_options_arg_t = option_id_t list
type cancel_expired_options_arg_t = investor_id_t
type terminate_options_arg = investor_id_t
```
