from pytezos.rpc.errors import MichelsonRuntimeError


def assert_is_not_in_big_map(ci, map_name, key):
    try:
        ci.big_map_get(f'{map_name}/{key}')
        assert 0
    except:
        assert 1

def _test_error_msg(cb, err_msg):
    try:
        cb()
        # call worked even though it should have failed => FAIL test
        assert 0
    except MichelsonRuntimeError as err:
        assert eval(err.__str__())[0]["with"]["string"] == err_msg


def _test_is_owner(ligo, tezos, entrypoints, err_msg, deploy = None, ci = None):
    """Tests that restricted entrypoints can only be called from contract owner."""
    if not ci:
        ci, _ = deploy(ligo, tezos)

    for name, params in entrypoints:
        entrypoint = getattr(ci, name)

        # assert that client will not be called from owner
        entrypoint.key = tezos.clients[1].key
        assert entrypoint.key.public_key_hash() != ci.storage()["owner"]

        cb = lambda: tezos.wait(entrypoint(params))
        _test_error_msg(cb, err_msg)


from pytezos.operation.fees import hard_gas_limit_per_operation

IS_POST_CARTHAGE = hard_gas_limit_per_operation > 1040000

gas_ops = {}
def map_contract_addr_to_type(env, addr_to_contract_type):
    global gas_ops
    new_gas_ops = gas_ops.copy()
    mapping = addr_to_contract_type(env)
    for old_key, v in gas_ops.items():
        if old_key in mapping.values():
            continue
        if old_key not in mapping.keys():
            continue
        new_key = mapping[old_key]
        if new_key not in new_gas_ops.keys():
            new_gas_ops[new_key] = []
        new_gas_ops[new_key] = new_gas_ops[new_key] + v
        del(new_gas_ops[old_key])
    gas_ops = {}
    return new_gas_ops
