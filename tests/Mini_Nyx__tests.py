import requests
from pytezos.michelson.pack import get_key_hash

from pynyx.contracts import MiniNyx, MiniNyxParams
from tests.pytest_tezos import tezos
from helpers import _test_is_owner, _test_error_msg, assert_is_not_in_big_map, map_contract_addr_to_type


gas_ops = []


def addr_to_contract_type(mini_nyx):
    return {
        mini_nyx.addr: 'mini_nyx',
    }


def test_entrypoint_permissions(ligo, tezos):
    """Tests that restricted entrypoints can only be called from contract owner."""
    err_msg = "31"

    entrypoints = [
        ["mint", {'amount': 12, 'tr_to': tezos.addresses[0]}],
        ["burn", {'amount': 12, 'tr_to': tezos.addresses[0]}],
        ["setAllowTransferFrom", {
            'authority': tezos.addresses[0],
            'transfer_from_info': {'transfer_for': tezos.addresses[0], 'amount': 1},
            'remove': False
            }
         ],
    ]

    params = MiniNyxParams(owner=tezos.addresses[0])
    mini_nyx = MiniNyx(ligo, tezos, params)
    _test_is_owner(ligo, tezos, entrypoints[:2], err_msg, ci=mini_nyx.ci)

    del(entrypoints[0])
    del(entrypoints[1])
    _test_is_owner(ligo, tezos, entrypoints[2:], 'You are not allowed to allow transfer from for this investor', ci=mini_nyx.ci)

    gas_ops.append(map_contract_addr_to_type(mini_nyx, addr_to_contract_type))

def test_happy_path(ligo, tezos):
    owner = tezos.addresses[0]
    params = MiniNyxParams(owner)
    mini_nyx = MiniNyx(ligo, tezos, params)

    inv_from, inv_to, amount = tezos.addresses[0], tezos.addresses[2], 10

    assert_is_not_in_big_map(mini_nyx.ci, 'balances', inv_from)
    assert_is_not_in_big_map(mini_nyx.ci, 'balances', inv_to)
    tezos.wait(mini_nyx.ci.mint({"tr_to": inv_from, "amount": amount}))
    assert mini_nyx.ci.big_map_get(f'balances/{inv_from}') == amount
    tezos.wait(mini_nyx.ci.transfer([{"tr_to": inv_to, "amount": amount}]))
    assert mini_nyx.ci.big_map_get(f'balances/{inv_from}') == 0
    assert mini_nyx.ci.big_map_get(f'balances/{inv_to}') == amount

    gas_ops.append(map_contract_addr_to_type(mini_nyx, addr_to_contract_type))


def test_mint(ligo, tezos):
    """Test that the issuer can mint tokens."""
    owner = tezos.addresses[0]
    inv = tezos.addresses[4]
    params = MiniNyxParams(owner)
    mini_nyx = MiniNyx(ligo, tezos, params)

    # Test that owner can mint to accredited investor
    def mint(amount = 12):
        try:
            balance_before = mini_nyx.ci.big_map_get('balances/'+inv)
        except:
            balance_before = 0
        total_supply_before = mini_nyx.ci.storage()['total_supply']
        tezos.wait(mini_nyx.ci.mint({'amount': amount, 'tr_to': inv}))
        assert mini_nyx.ci.big_map_get('balances/' + inv) == balance_before + amount
        assert mini_nyx.ci.storage()['total_supply'] == total_supply_before + amount


    # Minting should work
    mint()

    # Trying to mint more than is available
    cb = lambda: mint(mini_nyx.ci.storage()['tokens'] + 1)
    _test_error_msg(cb, "34")

    gas_ops.append(map_contract_addr_to_type(mini_nyx, addr_to_contract_type))


def test_burn(ligo, tezos):
    """Test that the issuer can burn tokens."""
    owner = tezos.addresses[0]
    inv = tezos.addresses[2]
    params = MiniNyxParams(owner)
    mini_nyx = MiniNyx(ligo, tezos, params)

    init_amount = 10
    tezos.wait(mini_nyx.ci.mint({'amount': init_amount, 'tr_to': inv}))

    # Test that owner can burn to accredited investor
    def burn(amount = 1):
        balance_before = mini_nyx.ci.big_map_get('balances/'+inv)
        total_supply_before = mini_nyx.ci.storage()['total_supply']
        tezos.wait(mini_nyx.ci.burn({'amount': amount, 'tr_to': inv}))
        assert mini_nyx.ci.big_map_get('balances/'+inv) == balance_before - amount
        assert mini_nyx.ci.storage()['total_supply'] == total_supply_before - amount

    # Burning should work
    burn(mini_nyx.ci.big_map_get('balances/'+inv))

    tezos.wait(mini_nyx.ci.mint({'amount': init_amount, 'tr_to': inv}))

    # Trying to burn more than is available
    cb = lambda: burn(mini_nyx.ci.big_map_get('balances/'+inv) + 1)
    _test_error_msg(cb, "33")

    gas_ops.append(map_contract_addr_to_type(mini_nyx, addr_to_contract_type))


def allow_transfer_from(tezos, mini_nyx, authority, transfer_for, amount, remove=False):
    set_allow_transfer_from_params = mini_nyx.make_set_allow_transfer_from_params(
        authority, transfer_for, amount, remove)

    tezos.wait(mini_nyx.ci.setAllowTransferFrom(set_allow_transfer_from_params))

    return set_allow_transfer_from_params['transfer_from_info']

def test_set_allow_transfer_from(ligo, tezos):
    """Tests that the owner can add an authority allowed to call transfer_from."""
    owner = tezos.addresses[0]
    authority_index = 4
    authority = tezos.addresses[authority_index]
    params = MiniNyxParams(owner)
    mini_nyx = MiniNyx(ligo, tezos, params)

    def set_allow_transfer_from(authority, transfer_for, amount, remove):
        allow_transfer_from(tezos, mini_nyx, authority, transfer_for, amount, remove)
        if remove:
            assert_is_not_in_big_map(mini_nyx.ci, 'allow_transfer_from', authority)
        else:
            assert mini_nyx.get_allowance_for(authority, transfer_for) == amount

    set_allow_transfer_from(authority, tezos.addresses[1], 1000, False)
    set_allow_transfer_from(authority, tezos.addresses[1], 1000, True)

    transfer_from_info = {
        'transfer_for': tezos.addresses[1],
        'amount': 1000,
    }
    set_allow_transfer_from_params = {
        'authority': authority,
        'transfer_from_info': transfer_from_info,
        'remove': False,
        'prev_amount': 123456789,
    }
    cb = lambda: tezos.wait(mini_nyx.ci.setAllowTransferFrom(set_allow_transfer_from_params))
    cb()  # we create an allowance so we can have "38" throw the next line
    # Should fail because prev_amount different
    _test_error_msg(cb, "38")

    gas_ops.append(map_contract_addr_to_type(mini_nyx, addr_to_contract_type))


def test_transfer(ligo, tezos):
    """Test that the issuer can transfer tokens."""
    sender_index = 0

    inv_from = tezos.addresses[sender_index]
    inv_to = tezos.addresses[1]

    owner = tezos.addresses[0]
    params = MiniNyxParams(owner)
    mini_nyx = MiniNyx(ligo, tezos, params)

    tezos.wait(mini_nyx.ci.mint({"tr_to": inv_from, "amount": 100}))

    # Test that accredited investor can transfer to accredited investor
    def transfer(amount = 22):
        try:
            balance_from_before = mini_nyx.ci.big_map_get('balances/'+inv_from)
        except:
            balance_from_before = 0

        try:
            balance_to_before = mini_nyx.ci.big_map_get('balances/'+inv_to)
        except:
            balance_to_before = 0

        tezos.wait(mini_nyx.ci.transfer([{
            'tr_to': inv_to,
            'amount': amount,
        }]))
        assert mini_nyx.ci.big_map_get('balances/'+inv_from) == balance_from_before - amount
        assert mini_nyx.ci.big_map_get('balances/'+inv_to) == balance_to_before + amount

    # Transferring should work
    transfer()

    # Trying to transfer more than is available
    cb = lambda: transfer(mini_nyx.ci.big_map_get('balances/'+inv_from) + 1)
    _test_error_msg(cb, "32")

    gas_ops.append(map_contract_addr_to_type(mini_nyx, addr_to_contract_type))


def test_transfer_from(ligo, tezos):
    """Test that the issuer can transfer tokens."""
    authority_index = 1
    tr_from_index = 0
    tr_to_index = 2
    extra_inv = tezos.addresses[3]

    authority = tezos.addresses[authority_index]
    inv_from = tezos.addresses[tr_from_index]
    inv_to = tezos.addresses[tr_to_index]

    owner_index = tr_from_index
    owner = tezos.addresses[owner_index]
    params = MiniNyxParams(owner)
    mini_nyx = MiniNyx(ligo, tezos, params)

    tezos.wait(mini_nyx.ci.mint({"tr_to": inv_from, "amount": 100}))

    authority_client = tezos.clients[authority_index]
    mini_nyx.ci.transferFrom.key = authority_client.key

    # Test that accredited investor can transfer to accredited investor
    def transfer_from(amount = 22, inv_from = inv_from):
        balance_from_before = mini_nyx.ci.big_map_get('balances/'+inv_from)
        try:
            balance_to_before = mini_nyx.ci.big_map_get('balances/'+inv_to)
        except:
            balance_to_before = 0

        tezos.wait(mini_nyx.ci.transferFrom([{
            'tr_from': inv_from,
            'tr_to': inv_to,
            'amount': amount,
        }]))

        assert mini_nyx.ci.big_map_get('balances/'+inv_from) == balance_from_before - amount
        assert mini_nyx.ci.big_map_get('balances/'+inv_to) == balance_to_before + amount

    ## Happy path

    amount = 20
    allow_transfer_from(tezos, mini_nyx, authority, inv_from, amount, False)
    transfer_from(amount)
    assert mini_nyx.get_allowance_for(authority, inv_from) == 0

    amount = 20
    allow_transfer_from(tezos, mini_nyx, authority, inv_from, amount, False)
    answer = int(amount / 2)
    transfer_from(answer)
    assert mini_nyx.get_allowance_for(authority, inv_from) == answer

    ## Fail cases

    cb = lambda: transfer_from(mini_nyx.ci.big_map_get('balances/'+inv_from) + 1)
    _test_error_msg(cb, "37")

    # Remove the authorization for authority
    allow_transfer_from(tezos, mini_nyx, authority, inv_from, amount, True)
    cb = lambda: transfer_from(amount, inv_from)
    _test_error_msg(cb, "36")


    ## Admin can use transferFrom without needing allowances
    mini_nyx.ci.transferFrom.key = tezos.clients[owner_index].key

    # assert there are no allowances
    # Should fail since no allowances exist
    try:
        mini_nyx.get_allowance_for(owner, inv_from)
        assert 0
    except:
        assert 1

    balances_before = {
        inv: mini_nyx.ci.big_map_get('balances/' + inv)
        for inv in [inv_from, inv_to]
    }
    transfer_from(amount, inv_from)  # from admin
    balances_after = {
        inv: mini_nyx.ci.big_map_get('balances/' + inv)
        for inv in [inv_from, inv_to]
    }
    assert balances_before[inv_from] - amount == balances_after[inv_from]
    assert balances_before[inv_to] == balances_after[inv_to] - amount

    gas_ops.append(map_contract_addr_to_type(mini_nyx, addr_to_contract_type))


def test_save_gas_costs():
    import json
    with open('gas/mini_nyx.json', 'w', encoding='utf-8') as f:
        json.dump(gas_ops, f, ensure_ascii=False, indent=4)
