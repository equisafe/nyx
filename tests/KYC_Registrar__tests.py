from datetime import datetime

from helpers import _test_is_owner
from pynyx.environments import KYCEnv as Env
from pynyx.helpers import get_id_for_region_code
from tests.pytest_tezos import tezos

from helpers import map_contract_addr_to_type


gas_ops = []

def addr_to_contract_type(env):
    return {
        env.kyc.addr: 'kyc',
    }

def test_is_owner(ligo, tezos):
    """Tests that restricted entrypoints can only be called from contract owner."""
    err_msg = "11"
    entrypoints = [
        ["addMembers", []],
        ["removeMembers", []],
        ["setMemberCountry", [tezos.addresses[1], get_id_for_region_code("FR-J")]],
    ]

    e = Env(ligo, tezos)
    _test_is_owner(ligo, tezos, entrypoints, err_msg, ci=e.kyc.ci)

    gas_ops.append(map_contract_addr_to_type(e, addr_to_contract_type))


def get_default_add_member_params(tezos):
    return [
        {
            "address_0": tezos.addresses[0],
            "country": get_id_for_region_code("FR-J"),
            "expires": 1245,
            "rating": 10,
            "region": 1,
            "restricted": False,
        },
        {
            "address_0": tezos.addresses[1],
            "country": get_id_for_region_code("FR-J"),
            "expires": 1245,
            "rating": 1,
            "region": 1,
            "restricted": True,
        },
        {
            "address_0": tezos.addresses[2],
            "country": get_id_for_region_code("FR-OCC"),
            "expires": 12345,
            "rating": 1,
            "region": 2,
            "restricted": True,
        },
        {
            "address_0": tezos.addresses[3],
            "country": get_id_for_region_code("FR-OCC"),
            "expires": 12345,
            "rating": 1,
            "region": 3,
            "restricted": False,
        },
        {
            "address_0": tezos.addresses[4],
            "country": get_id_for_region_code("FR-OCC"),
            "expires": 1245,
            "rating": 10,
            "region": 1,
            "restricted": False,
        },
    ]


def test_add_members(ligo, tezos):
    """Tests that contract owner can add and modify investors."""
    e = Env(ligo, tezos)

    add_members_params = get_default_add_member_params(tezos)

    tezos.wait(e.kyc.ci.addMembers(add_members_params))
    assert e.kyc.ci.addMembers.key.public_key_hash() == e.kyc.ci.storage()["owner"]

    def get_answer(address):
        answer = {
            m["address_0"]: {
                k: v for k, v in m.items()
                if k != "address_0"
            }
            for m in add_members_params
            if m["address_0"] == address
        }
        for m in answer.values():
            m["expires"] = (
                datetime.utcfromtimestamp(m["expires"]).__str__().replace(" ", "T")
                + "Z"
            )
        return answer[address]

    # Compare big map entries individually
    assert [
        e.kyc.ci.big_map_get(a) == get_answer(a)
        for a in [
                tezos.addresses[i]
                for i in range(len(add_members_params))
                ]
        ] == [True for i in range(len(add_members_params))]

    # updating investor
    add_members_params[0]["country"] = get_id_for_region_code("FR-H")
    assert (e.kyc.ci.big_map_get(add_members_params[0]["address_0"])["country"] != add_members_params[0]["country"])
    tezos.wait(e.kyc.ci.addMembers(add_members_params))

    assert [
        e.kyc.ci.big_map_get(a) == get_answer(a)
        for a in [
                tezos.addresses[i]
                for i in range(len(add_members_params))
                ]
        ] == [True for i in range(len(add_members_params))]

    gas_ops.append(map_contract_addr_to_type(e, addr_to_contract_type))


def test_remove_members(ligo, tezos):
    """Tests that contract owner can remove investors."""
    e = Env(ligo, tezos)

    add_members_params = get_default_add_member_params(tezos)
    tezos.wait(e.kyc.ci.addMembers(add_members_params))
    assert e.kyc.ci.addMembers.key.public_key_hash() == e.kyc.ci.storage()["owner"]
    remove_members_params = [tezos.addresses[1], tezos.addresses[2]]
    tezos.wait(e.kyc.ci.removeMembers(remove_members_params))

    def get_answer(address):
        answer = {
            m["address_0"]: {
                k: v for k, v in m.items()
                if k != "address_0"
            }
            for m in add_members_params
            if m["address_0"] not in remove_members_params
            and m["address_0"] == address
        }
        for m in answer.values():
            m["expires"] = (
                datetime.utcfromtimestamp(m["expires"]).__str__().replace(" ", "T")
                + "Z"
            )
        return answer[address]

    assert [
        e.kyc.ci.big_map_get(a) == get_answer(a)
        for a in [
                tezos.addresses[i]
                for i in range(len(add_members_params))
                if tezos.addresses[i] not in remove_members_params
            ]
        ] == [True for  m in add_members_params if m["address_0"] not in remove_members_params]

    gas_ops.append(map_contract_addr_to_type(e, addr_to_contract_type))


def test_set_member_country(ligo, tezos):
    """Tests that owner can set member countries."""
    inv, old_country = tezos.addresses[1], get_id_for_region_code("FR-OCC")
    e = Env(ligo, tezos).setup_env([(inv, old_country, 12345, 10, 1, False)])

    new_country = get_id_for_region_code("FR-E")
    assert new_country != old_country
    tezos.wait(e.kyc.ci.setMemberCountry([inv, new_country]))
    assert e.kyc.ci.big_map_get(inv)["country"] == new_country

    gas_ops.append(map_contract_addr_to_type(e, addr_to_contract_type))

def test_save_gas_costs():
    import json
    with open('gas/kyc.json', 'w', encoding='utf-8') as f:
        json.dump(gas_ops, f, ensure_ascii=False, indent=4)
