from pynyx.environments import EventSinkEnv as Env
from helpers import _test_error_msg, _test_is_owner, map_contract_addr_to_type
from tests.pytest_tezos import tezos


gas_ops = []

def addr_to_contract_type(env):
    return {
        env.event_sink.addr: 'event_sink',
    }

def test_is_owner(ligo, tezos):

    """Tests that restricted entrypoints can only be called from contract owner."""
    err_msg = "51"
    entrypoints = [
        ["flushMessages", None],
        ["setPermission", [tezos.addresses[0], True]],
    ]

    e = Env(ligo, tezos)
    _test_is_owner(ligo, tezos, entrypoints, err_msg, ci=e.event_sink.ci)

    gas_ops.append(map_contract_addr_to_type(e, addr_to_contract_type))

def test_set_permission(ligo, tezos):
    e = Env(ligo, tezos)
    contract_index = 4
    authorized_contract = tezos.addresses[contract_index]
    perms = [authorized_contract, True]
    assert authorized_contract not in e.event_sink.ci.storage()['permissions']
    tezos.wait(e.event_sink.ci.setPermission(perms))
    assert authorized_contract in e.event_sink.ci.storage()['permissions']

    perms[1] = False
    tezos.wait(e.event_sink.ci.setPermission(perms))
    assert authorized_contract not in e.event_sink.ci.storage()['permissions']

    gas_ops.append(map_contract_addr_to_type(e, addr_to_contract_type))

def test_post_event(ligo, tezos):
    e = Env(ligo, tezos)
    owner_index = 0
    contract_index = 4
    authorized_contract = tezos.addresses[contract_index]


    # authorized_contract is not yet allowed to post
    # so this should fail
    e.event_sink.switch_key(tezos.clients[contract_index])
    msg = "foo"
    _test_error_msg(
        lambda: tezos.wait(e.event_sink.ci.postEvent(msg)),
        "52"
    )

    # we authorize the contract to post events
    e.event_sink.switch_key(tezos.clients[owner_index])
    perms = [authorized_contract, True]
    tezos.wait(e.event_sink.ci.setPermission(perms))

    # and assert that it ca
    e.event_sink.switch_key(tezos.clients[contract_index])
    tezos.wait(e.event_sink.ci.postEvent(msg))
    event = e.event_sink.ci.big_map_get('messages/' + str(e.event_sink.ci.storage()['message_counter']))
    assert event['contract'] == authorized_contract
    assert event['message'] == msg

    gas_ops.append(map_contract_addr_to_type(e, addr_to_contract_type))

def test_flush_messages(ligo, tezos):
    e = Env(ligo, tezos)
    owner_index = 0
    contract_index = 4
    authorized_contract = tezos.addresses[contract_index]

    # we authorize the contract to post events
    perms = [authorized_contract, True]
    tezos.wait(e.event_sink.ci.setPermission(perms))

    # We post an event
    msg = "foo"
    e.event_sink.switch_key(tezos.clients[contract_index])
    tezos.wait(e.event_sink.ci.postEvent(msg))

    # We flush it from the owner address
    e.event_sink.switch_key(tezos.clients[owner_index])
    tezos.wait(e.event_sink.ci.flushMessages(None))

    # the message counter is back at 0 and the big map is empty
    assert e.event_sink.ci.storage()['message_counter'] == 0
    try:
        e.event_sink.ci.big_map_get('messages/' + e.event_sink.ci.storage()['message_counter'])
        assert 0
    except:
        assert 1

    gas_ops.append(map_contract_addr_to_type(e, addr_to_contract_type))

def test_save_gas_costs():
    import json
    with open('gas/event_sink.json', 'w', encoding='utf-8') as f:
        json.dump(gas_ops, f, ensure_ascii=False, indent=4)
