from typing import List
from decimal import Decimal

from pynyx.environments import CrowdsaleEnv, EnvParams, TokenEnv, ExchangeEnv
from pynyx.contracts import TokenParams, CrowdsaleParams, ExchangeParams

from Security_Tokens__tests import allow_transfer_from
from helpers import IS_POST_CARTHAGE
from pynyx.helpers import ZERO_ADDRESS
from tests.pytest_tezos import tezos


class Env(CrowdsaleEnv):
    def __init__(
            self,
            ligo,
            tezos,
            params = None,
        ):
        super().__init__(ligo, tezos, params, ParentEnv=TokenEnv)

    def setup_env(self, kyc_specs = []):
        super().setup_env(kyc_specs)
        return self


class ExchangeEnv(ExchangeEnv):
    def setup_env(self, fiat_rates: List[int], tokens_maxs: List[int], token_owner_index: int):
        amount, receiver = 50, self.exchange.params.receiver

        # Mint tokens to organizer
        for i, token_env in enumerate(self.token_envs):
            token_env.token.switch_key(self.tezos.clients[token_owner_index])
            self.tezos.wait(token_env.token.ci.mint({'amount': amount, 'tr_to': receiver}))

        # Setup transfer from
        for token_env in self.token_envs:
            allow_transfer_from(token_env, self.exchange.addr, receiver, amount)

        # Add tokens to exchange
        super().setup_env(fiat_rates, tokens_maxs, 3, 100)

        return self

def test_exchange(ligo, tezos):
    indices = [1, 0, 3]
    organizer_index, investor_index, token_owner_index = indices
    organizer, investor, token_owner = [tezos.addresses[i] for i in indices]

    token_env_params = EnvParams(
        Token=TokenParams(owner=token_owner, standalone=True),
    )
    token_envs = [TokenEnv(ligo, tezos, token_env_params) for i in range(3)]

    exchange_env_params = EnvParams(
        Exchange=ExchangeParams(owner=organizer, receiver=organizer),
    )
    e = ExchangeEnv(ligo, tezos, exchange_env_params, token_envs=token_envs)
    e.setup_env([2, 3, 4], [100, 200, 300], token_owner_index)

    amount = 10
    token_envA, token_envB = [e.token_envs[i] for i in range(2)]

    # Mint some tokens to investor
    token_envA.token.switch_key(tezos.clients[token_owner_index])
    tezos.wait(token_envA.token.ci.mint({'amount': amount, 'tr_to': investor}))

    # Save storages for balance assertions after exchanging between currencies
    #token_envA_storage_before = token_envA.token.ci.storage()
    #token_envB_storage_before = token_envB.token.ci.storage()
    def balances_safe_get(ci, account):
        try:
            return ci.big_map_get('balances/' + account)
        except:
            return 0

    token_balances_before = {
        token_name: {
            account: balances_safe_get(token_env.token.ci, account)
            for account in [investor, organizer]
        }
        for token_name, token_env in [('A', token_envA), ('B', token_envB)]
    }

    # Allow contract to transfer `amount` form investor to itself
    token_envA.token.switch_key(tezos.clients[investor_index])
    allow_transfer_from(token_envA, e.exchange.addr, investor, amount)
    # Exchange `amount`
    e.exchange.switch_key(tezos.clients[investor_index])
    tezos.wait(e.exchange.ci.payable(token_envA.token.addr, token_envB.token.addr, amount))

    fiat_rateA = e.exchange.ci.storage()['currencies'][token_envA.token.addr]['fiat_rate']
    fiat_rateB = e.exchange.ci.storage()['currencies'][token_envB.token.addr]['fiat_rate']
    delta_currB = int(amount * fiat_rateA / fiat_rateB)
    assert token_envA.token.ci.big_map_get('balances/'+investor) == token_balances_before['A'][investor] - amount
    if investor in token_balances_before['A'].keys():
        assert token_envB.token.ci.big_map_get('balances/'+investor) == token_balances_before['B'][investor] + delta_currB
    else:
        assert token_envB.token.ci.big_map_get('balances/'+investor) == delta_currB
    assert token_envA.token.ci.big_map_get('balances/'+organizer) == token_balances_before['A'][organizer] + amount
    assert token_envB.token.ci.big_map_get('balances/'+organizer) == token_balances_before['B'][organizer] - delta_currB

    organizer_tz_before = tezos.clients[organizer_index].balance()
    investor_token_before = balances_safe_get(token_envB.token.ci, investor)
    organizer_token_before = token_envB.token.ci.big_map_get('balances/'+organizer)

    pay_with, _ = 5, 0

    tezos.wait(e.exchange.ci.payable(ZERO_ADDRESS, token_envB.token.addr, _).with_amount(Decimal(pay_with)))

    organizer_tz_after = tezos.clients[organizer_index].balance()
    assert organizer_tz_after - organizer_tz_before == Decimal(pay_with)

    investor_token_after = token_envB.token.ci.big_map_get('balances/'+investor)
    organizer_token_after = token_envB.token.ci.big_map_get('balances/'+organizer)

    fiat_rateTZ = e.exchange.ci.storage()['currencies'][ZERO_ADDRESS]['fiat_rate']
    delta_currB = int(pay_with * fiat_rateTZ / fiat_rateB)
    assert investor_token_after - investor_token_before == delta_currB
    assert organizer_token_after - organizer_token_before == -delta_currB


def skip_test_standalone_token(ligo, tezos):
    organizer_index = 4
    organizer = tezos.addresses[organizer_index]
    investor_index = 3
    investor = tezos.addresses[investor_index]

    params = EnvParams(
        Token=TokenParams(owner=organizer, standalone=True),
        Crowdsale=CrowdsaleParams(owner=organizer),
    )
    e = Env(ligo, tezos, params)

    tezos.wait(e.token.ci.transfer({
        'tr_to': organizer,
        'amount': 100,
    }))

    crowdsale, amount = e.crowdsale.addr, 100

    e.token.switch_key(tezos.clients[organizer_index])
    allow_transfer_from(e, crowdsale, organizer, amount)

    e.crowdsale.switch_key(e.tezos.clients[investor_index])

    token_fiat_rate = e.crowdsale.ci.storage()['token_fiat_rate']
    tz_fiat_rate = e.crowdsale.ci.storage()['tz_fiat_rate']

    organizer_tz_before = tezos.clients[organizer_index].balance()
    investor_token_before = 0 if investor not in e.token.ci.storage()['balances'].keys() else e.token.ci.storage()['balances'][investor]
    organizer_token_before = e.token.ci.storage()['balances'][organizer]
    crowdsale_storage_before = e.crowdsale.ci.storage()

    pay_with = 5  # tz
    resp = tezos.wait(e.crowdsale.ci.payable(None).with_amount(Decimal(pay_with)))

    debitor = resp['contents'][0]['metadata']['operation_result']['balance_updates'][0]
    creditor = resp['contents'][0]['metadata']['operation_result']['balance_updates'][1]
    delta = pay_with * 1e6  # mutez
    assert debitor['contract'] == investor
    assert debitor['change'] ==  str(int(-delta))
    assert creditor['contract'] == crowdsale
    assert creditor['change'] == str(int(delta))

    organizer_tz_after = tezos.clients[organizer_index].balance()
    investor_token_after = e.token.ci.storage()['balances'][investor]
    organizer_token_after = e.token.ci.storage()['balances'][organizer]
    assert investor_token_after - investor_token_before == int(pay_with * tz_fiat_rate / token_fiat_rate)
    assert organizer_token_before - organizer_token_after == int(pay_with * tz_fiat_rate / token_fiat_rate)
    # the autobaker bakes on all bootstrap addresses, so sometimes there is extra tz from baking the
    # 'payable' transaction above. We use '>=' to prevent this
    assert organizer_tz_after - organizer_tz_before >= Decimal(pay_with)

    assert e.crowdsale.ci.storage()['fiat'] == int(crowdsale_storage_before['fiat'] + pay_with * tz_fiat_rate)
    assert e.crowdsale.ci.storage()['tokens'] == int(crowdsale_storage_before['tokens'] + pay_with * tz_fiat_rate / token_fiat_rate)


def skip_test_security_token(ligo, tezos):
    if not IS_POST_CARTHAGE:
        # skip test on master branche for now
        # TODO move test to post-carthage branch only
        return True

    organizer_index = 4
    organizer = tezos.addresses[organizer_index]
    investor_index = 3
    investor = tezos.addresses[investor_index]
    e = Env(ligo, tezos).setup_env([organizer, investor, tezos.addresses[0]])

    crowdsale, amount = e.crowdsale.addr, 100
    allow_transfer_from(e, crowdsale, organizer, amount)

    tezos.wait(e.token.ci.transfer({
        'tr_to': organizer,
        'amount': amount,
    }))

    investor_client = e.tezos.clients[investor_index]
    e.crowdsale.ci.payable.key = investor_client.key

    token_fiat_rate = e.crowdsale.ci.storage()['token_fiat_rate']
    tz_fiat_rate = e.crowdsale.ci.storage()['tz_fiat_rate']

    organizer_tz_before = tezos.clients[organizer_index].balance()
    investor_token_before = 0 if investor not in e.token.ci.storage()['balances'].keys() else e.token.ci.storage()['balances'][investor]
    organizer_token_before = e.token.ci.storage()['balances'][organizer]
    crowdsale_storage_before = e.crowdsale.ci.storage()

    pay_with = 5  # tz
    resp = tezos.wait(e.crowdsale.ci.payable(None).with_amount(Decimal(pay_with)))

    debitor = resp['contents'][0]['metadata']['operation_result']['balance_updates'][0]
    creditor = resp['contents'][0]['metadata']['operation_result']['balance_updates'][1]
    delta = pay_with * 1e6  # mutez
    assert debitor['contract'] == investor
    assert debitor['change'] ==  str(int(-delta))
    assert creditor['contract'] == crowdsale
    assert creditor['change'] == str(int(delta))

    organizer_tz_after = tezos.clients[organizer_index].balance()
    investor_token_after = e.token.ci.storage()['balances'][investor]
    organizer_token_after = e.token.ci.storage()['balances'][organizer]
    assert investor_token_after - investor_token_before == int(pay_with * tz_fiat_rate / token_fiat_rate)
    assert organizer_token_before - organizer_token_after == int(pay_with * tz_fiat_rate / token_fiat_rate)
    # the autobaker bakes on all bootstrap addresses, so sometimes there is extra tz from baking the
    # 'payable' transaction above. We use '>=' to prevent this
    assert organizer_tz_after - organizer_tz_before >= Decimal(pay_with)

    assert e.crowdsale.ci.storage()['fiat'] == int(crowdsale_storage_before['fiat'] + pay_with * tz_fiat_rate)
    assert e.crowdsale.ci.storage()['tokens'] == int(crowdsale_storage_before['tokens'] + pay_with * tz_fiat_rate / token_fiat_rate)

