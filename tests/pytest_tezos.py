# modified from https://yourlabs.io/oss/pytest-tezos/-/blob/5282ced8a9afc50fe82a406eca5764d57676800c/pytest_tezos/__init__.py

import pytest
import os
from pytezos import pytezos

import helpers

from os.path import expanduser, join
import json


sandbox_ids = (
    'edsk3QoqBuvdamxouPhin7swCvkQNgq4jP5KZPbwWNnwdZpSpJiEbq',
    'edsk3RFfvaFaxbHx8BMtEW1rKQcPtDML3LXjNqMNLCzC3wLC1bWbAt',
    'edsk3EQB2zJvvGrMKzkUxhgERsy6qdDDw19TQyFWkYNUmGSxXiYm7Q',
    'edsk4YDWx5QixxHtEfp5gKuYDd1AZLFqQhmquFgz64mDXghYYzW6T9',
    'edsk3G87qnDZhR74qYDFAC6nE17XxWkvPJtWpLw4vfeZ3otEWwwskV',
)


class Tezos:
    def __init__(self):
        self.addresses = []
        self.clients = []
        for i in sandbox_ids:
            key = pytezos.key.from_encoded_key(i)
            self.addresses.append(key.public_key_hash())
            #host = 'tz' if os.getenv('CI') else 'localhost'
            host = 'tz'
            self.clients.append(pytezos.using(
                key=key,
                shell=f'http://{host}:20000',
            ))
        self.client = self.clients[0]

    def wait(self, origination):
        if type(origination).__name__ == 'ContractCall':
            origination = origination.inject()

        import time
        tries = 15
        while tries:
            try:
                op = self.clients[0].shell.blocks[-5:].find_operation(origination['hash'])
                #consumed_gas = op['contents'][0]['metadata']['operation_result']['consumed_gas']
                consumed_gas = op['contents'][0]['gas_limit']
                try:
                    entrypoint = origination['contents'][0]['parameters']['entrypoint']
                    contract_addr = op['contents'][0]['destination']
                    helpers.gas_ops[contract_addr].append((entrypoint, consumed_gas))
                except:
                    entrypoint = "__origination__"
                    contract_addr = op['contents'][0]['metadata']['operation_result']['originated_contracts'][0]
                    info = (entrypoint, consumed_gas)
                    helpers.gas_ops[contract_addr] = [info]

                return op
            except:
                time.sleep(1)
                tries -= 1
                if not tries:
                    raise

    def contract_address(self, origination):
        result = self.wait(origination)['contents'][0]['metadata']['operation_result']
        return result['originated_contracts'][0]


@pytest.fixture(scope="session")
def tezos():
    return Tezos()
