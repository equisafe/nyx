import re
import os
from subprocess import Popen, PIPE
from dataclasses import dataclass

from AbstractTest import AbstractTest, autobake
from TestTypes import Address, Tuple, Map, Set, String, AbstractRecord, Bool, Nat, Timestamp, List, Bytes, Unit


# Defining empty data structures. They are typed in cameligo
# but not in michelson, so we're just defining them by the
# simplest cameligo typings.
empty_set = lambda: Set(set([]), "int")
empty_map = lambda: Map({}, "int", "int")


@dataclass
class ModuleStorage(AbstractRecord):
    value: Nat
    owner: Address

    # why is this needed ? wtf ?
    def __repr__(self):
        return super().__repr__()


class ModuleTests(AbstractTest):
    CONTRACT_NAME = "MODULE"
    SRC_PATH = "../modules/double_add_module.mligo"

    def __init__(self, module_owner: str):
        self.module_owner = Address(module_owner)

        super().__init__()

    def assert_storage_eq(self):
        cond = self.get_storage() == self.storage_obj.get_michelson(self.compile_expr)[1:-1]
        assert(cond)
        return cond

    @property
    def init_storage(self):
        self.storage_obj = ModuleStorage(Nat(0), self.module_owner)

        return self.storage_obj.__repr__()



@dataclass
class HookInfo(AbstractRecord):
    tag_bools: List
    permitted: Bool
    active: Bool
    always: Bool

    # why is this needed ? wtf ?
    def __repr__(self):
        return super().__repr__()

@dataclass
class ModuleInfo(AbstractRecord):
    active: Bool
    set: Bool
    hooks: Map
    permissions: Map

    # why is this needed ? wtf ?
    def __repr__(self):
        return super().__repr__()

@dataclass
class OwnerStorage(AbstractRecord):
    value: Nat
    active_modules: Set
    module_data: Map

    # why is this needed ? wtf ?
    def __repr__(self):
        return super().__repr__()




class OwnerTests(AbstractTest):
    CONTRACT_NAME = "OWNER"
    SRC_PATH = "../modules/increment.mligo"

    def add_module(self, module):
        self.module = Address(module)

    def assert_storage_eq(self):
        cond = self.get_storage() == self.storage_obj.get_michelson(self.compile_expr)[1:-1]
        assert(cond)
        return cond

    @property
    def init_storage(self):
        self.storage_obj = OwnerStorage(Nat(0), empty_set(), empty_map())

        return self.storage_obj.__repr__()

    def increment(self, from_addr: str):
        """Calls increment"""
        self.call_contract(
            from_addr,
            self.contract_name,
            "increment",
            "Unit")
            #Unit().get_michelson(self.compile_expr))

    @autobake
    def test_11_increment(self):
        """Increments value by 1 regardless of whether the module is attached or not."""
        self.increment(self.owner)

        self.storage_obj.value += Nat(1)

        self.assert_storage_eq()

    def modular_increment(self, from_addr: str):
        """Calls modular increment."""
        self.call_contract(
            from_addr,
            self.contract_name,
            "modularIncrement",
            "Unit")
            #Unit().get_michelson(self.compile_expr))

    @autobake
    def test_21_modular_increment(self):
        """Increments value by 1 since module is not attached yet."""
        self.modular_increment(self.owner)

        self.storage_obj.value += Nat(1)

        self.assert_storage_eq()

    def activate_module(self, from_addr: str, module: Address):
        """Calls activate module"""
        self.call_contract(
            from_addr,
            self.contract_name,
            "activateModule",
            module.get_michelson(self.compile_expr))

    @autobake
    def test_31_activate_module(self):
        """Activates the module and sets up hooks and permissions."""
        self.activate_module(self.owner, self.module)

        self.storage_obj.active_modules.value.add(self.module)

        permissions = Map({}, "string",  "bool")
        hook_info = HookInfo(List([], "bool"), Bool(True), Bool(True), Bool(True))
        hooks = Map({ String("modularIncrement"): hook_info })
        module_info = ModuleInfo(Bool(True), Bool(True), hooks, permissions)
        self.storage_obj.module_data.value[self.module] = module_info

        self.assert_storage_eq()

    @autobake
    def test_41_modular_increment(self):
        """Increments value by 2 since module is attached."""
        self.modular_increment(self.owner)

        self.storage_obj.value += Nat(2)

        self.assert_storage_eq()

if __name__ == "__main__":
    owner = OwnerTests().deploy_contract()
    module = ModuleTests(owner.contract_addr).deploy_contract()

    owner.add_module(module.contract_addr)
    owner.run_all_test_methods()

    print("\nSuccess :)")
