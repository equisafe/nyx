#+begin_src python :session issuer :exports none
import sys ; sys.path.append("..")
import os ; os.chdir("..")
#+end_src

#+RESULTS:

#+BEGIN_SRC ditaa :file ../docs/images/issuer.png :exports results
+--------------+                +--------------+
|              |                |              |
|      KYC     +<-------------->+    Issuer    |
|              |                |              |
+-------+------+                +-------+------+
        |                               |
        |                               :
        |                               |
        |                               v
        |                       +-------+------+
        |                       |              |
        +---------=------------>+  Event Sink  |
                                |              |
                                +--------------+

#+END_SRC

#+RESULTS:
[[file:../docs/images/issuer.png]]

The Issuing Entity contract keeps track of investor right with respect to asset transfers. When transferring a token for instance (see next section), the issuing entity is queried to check that the transfer is allowed. In particular, it is the Issuing Entity contract's job to query the various KYC Registrars associated with it for investor restrictions.

We can deploy an Issuing Entity with a KYC Registrar as follows:

#+begin_src python :session issuer :results output :exports both
# Import Ligo and Tezos helper classes
from pytest_ligo import Ligo
from pytest_tezos import Tezos
tezos = Tezos()

from pynyx.environments import IssuerEnv, EnvParams
from pynyx.contracts import IssuerParams, ContractParams

owner = tezos.addresses[0]
kyc_params = ContractParams(owner)
issuer_params = IssuerParams(owner=owner, name="IssuerContract")
env_params = EnvParams(KYC=kyc_params, Issuer=issuer_params)

tezos = Tezos()
nyx = IssuerEnv(Ligo(), tezos, env_params)
#+end_src

#+RESULTS:


#+begin_src python :session issuer :results output :exports both
nyx.log_storage()
#+end_src

#+RESULTS:
#+begin_example
=== KYC storage ===

{'members': 2984, 'owner': 'tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx'}


=== Issuer storage ===

{ 'accounts': 2985,
  'country_counters': {},
  'country_restrictions': 2986,
  'document_hashes': 2987,
  'global_invest_limit': 0,
  'investor_counter': 0,
  'kyc_registrars': {},
  'name': 'IssuerContract',
  'owner': 'tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx',
  'security_tokens': {}}


<pynyx.environments.IssuerEnv object at 0x7f65b4f07c10>
#+end_example

As you can see, the issuer has three big map entries in its storage record: ~accounts~, ~country_restrictions~ and ~document_hashes~. We can query each of the with ~nyx.issuer.ci.big_map_get~ as was done in the KYC contract.
