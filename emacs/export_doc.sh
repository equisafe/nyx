#!/bin/bash

for filename in *.org; do
    base=$(echo $filename | cut -f 1 -d '.')
    out="../docs/$base.md"
    pandoc -f org -t gfm $filename > $out
done
