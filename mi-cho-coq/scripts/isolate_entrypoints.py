from shutil import copyfile
from pathlib import Path
import os
import subprocess

spec = {
    "Mini_Nyx": ["Transfer", "Burn", "Mint", "SetAllowTransferFrom", "TransferFrom",],
    "KYC_Registrar": ["AddMembers", "RemoveMembers", "GetMember", "CheckMember", "SetMemberCountry"]
}

def copy_base_import_files(out_folder_path):
    Path(out_folder_path).mkdir(parents=True, exist_ok=True)

    base_in_path = "../../contracts/"
    filenames = ("base_types", "helpers")
    ext = ".mligo"

    for filename in filenames:
        in_path = os.path.abspath(base_in_path + filename + ext)
        out_path = os.path.abspath(out_folder_path + filename + ext)
        copyfile(in_path, out_path)

def remove_base_import_files(base_path):
    filenames = ("base_types", "helpers")
    ext = ".mligo"

    for filename in filenames:
        path = os.path.abspath(base_path + filename + ext)
        os.remove(path)

def compile_ligo_file(ligo_file_path, format="text"):
    current_dir = os.getcwd()
    os.chdir(os.path.abspath("../entrypoints/"))

    bashCommand = f"ligo compile-contract --michelson-format={format} {ligo_file_path} main"
    process = subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)
    result, error = process.communicate()
    tz_file_path = ligo_file_path.split(".")[0] + ".tz"
    with open(tz_file_path, "w") as f:
        f.write(result.decode("utf-8"))

    os.chdir(current_dir)

def isolate_entrypoint(contract_name, entrypoint_name):
    ext = ".mligo"
    types_ext = "_types" + ext
    base_input_path = "../../contracts/"

    base_output_path = "../entrypoints/" + contract_name + "/" + entrypoint_name + "/"
    Path(os.path.abspath(base_output_path)).mkdir(parents=True, exist_ok=True)

    copy_base_import_files(base_output_path)

    implementation, types = "implementation", "types"
    paths = {
        "input": {
            implementation: os.path.abspath(base_input_path + filename + ext),
            types: os.path.abspath(base_input_path + filename + types_ext),
        },
        "output": {
            implementation: os.path.abspath(base_output_path + filename + ext),
            types: os.path.abspath(base_output_path  + filename + types_ext),
        },
    }

    for kind in [implementation, types]:
        with open(paths["input"][kind]) as f:
            read_data = f.read()
            lines = read_data.split("\n")
            new_lines = []

            for line in lines:
                counter = 0
                entrypoints_to_remove = [e for e in entrypoints if e != entrypoint_name]
                for entrypoint in entrypoints_to_remove:
                    target = "| " + entrypoint + " "
                    if target not in line:
                        counter += 1
                if counter == len(entrypoints_to_remove):
                    new_lines.append(line)

            with open(paths["output"][kind], "w") as g:
                g.write("\n".join(new_lines))

    compile_ligo_file(paths["output"][implementation])

    os.remove(paths["output"][implementation])
    os.remove(paths["output"][types])
    remove_base_import_files(base_output_path)

for contract_name, entrypoints in spec.items():
    print("\n---> " + contract_name)
    for entrypoint in entrypoints:
        print(" |-> " + entrypoint)
        isolate_entrypoint(contract_name, entrypoint)
