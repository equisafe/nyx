Require Import Michocoq.macros.
Import syntax.
Import comparable.
Require Import NArith.
Require Import Michocoq.semantics.
Require Import Michocoq.util.
Import error.
Require List.


Definition parameter_ty := (or unit mutez).
Definition storage_ty := address.

Module ST : (SelfType with Definition self_type := parameter_ty).
  Definition self_type := parameter_ty.
End ST.

Module deposit(C:ContractContext)(E:Env ST C).

Module semantics := Semantics ST C E. Import semantics.

Definition deposit : full_contract _ ST.self_type storage_ty :=
  ( DUP;; CAR;; DIP1 CDR;;
    IF_LEFT
      ( DROP1;; NIL operation )
      ( DIP1 ( DUP;;
               DUP;; SENDER;; COMPARE;; EQ;; IF NOOP FAILWITH;;
               CONTRACT unit;; IF_NONE FAILWITH NOOP);;
        PUSH unit Unit;; TRANSFER_TOKENS;;
        NIL operation;; SWAP;; CONS);;
    PAIR ).

Lemma deposit_correct :
  forall (input : data (or unit mutez)) storage_in
         (ops : data (list operation)) storage_out
         (fuel : Datatypes.nat),
  fuel >= 42 ->
  eval env deposit fuel ((input, storage_in), tt) = Return ((ops, storage_out), tt)
  <->
  (storage_in = storage_out /\
   match input with
   | inl tt => ops = nil
   | inr am => (storage_in = sender env /\
                exists c : data (contract unit),
                  contract_ env unit storage_in = Some c /\
                  ops = cons (transfer_tokens env unit tt am c) nil)
   end).
Proof.
  intros input storage_in ops storage_out fuel Hfuel.
  rewrite return_precond.
  unfold eval.
  rewrite eval_precond_correct.
  unfold ">=" in Hfuel.
  simpl.
  more_fuel ; simpl.
  more_fuel ; simpl.
  more_fuel ; simpl.
  more_fuel ; simpl.
  more_fuel ; simpl.
 (* do 3 (more_fuel ; simpl). *)
  destruct input as [[]|am].
  - more_fuel ; simpl.  
    more_fuel ; simpl.
    intuition congruence.
  - do 11 (more_fuel ; simpl).
    rewrite if_false_is_and.
    rewrite (eqb_eq address).
    destruct (contract_ env unit storage_in).
    + split.
      * intros (Hsend, Hops).
        subst storage_in.
        injection Hops; intros; subst; clear Hops.
        do 2 (split; [reflexivity|]).
        exists d; split; reflexivity.
      * intros (Hstorage, (Hsend, (c, (Hcd, Hops)))).
        intuition congruence.
    + split.
      * intuition.
      * intros (_, (_, (c, (Habs, _)))).
        discriminate.
Qed.

End deposit.

