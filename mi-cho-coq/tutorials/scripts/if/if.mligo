type security_token_storage_t = {
  owner    : address ;
  balances : ( address , nat ) big_map ;
  tokens   : nat ;
}

type security_token_ret_t = (operation list * security_token_storage_t)

let main (param, s : unit * security_token_storage_t)
  : security_token_ret_t =
  let check_sender =
    if (
      sender <> s.owner
    ) then failwith "error"
  in
  ( ([] : operation list) , s )
