Require Import Michocoq.macros.
Import Michocoq.syntax.
Require Import Michocoq.semantics.
Require Import Michocoq.util.
Import error.
Require Import ZArith.
Import comparable.

Definition amount_ty := nat.
Definition destination_address_ty := address.
Definition parameter_ty := (pair amount_ty destination_address_ty).

Definition balances_ty := big_map address nat.
Definition owner_ty := address.
Definition supply_ty := nat.
Definition storage_ty := (pair (pair balances_ty owner_ty) supply_ty).

Module ST : (SelfType with Definition self_type := parameter_ty).
  Definition self_type := parameter_ty.
End ST.

Module if_contract(C:ContractContext)(E:Env ST C).

Module semantics := Semantics ST C E. Import semantics.

Import List.ListNotations.

Definition if_contract : full_contract _ ST.self_type storage_ty :=
       ( DUP ;;
         CDR ;;
         DUP ;;
         CAR ;;
         CDR ;;
         SENDER ;;
         COMPARE ;;
         NEQ ;;
         IF ( FAIL ) ( UNIT ) ;;
         DIG (S1 := [_]) 1 eq_refl ;;
         DUP ;;
         DUG (S1 := [_; _]) 2 eq_refl ;;
         NIL operation ;;
         PAIR ;;
         DIP1 ( DROP (A := [_; _; _]) 3 eq_refl) ).

Lemma address_compare_Eq : forall a : address_constant,
  address_compare a a = Eq.
Proof.
  intros a. induction a. simpl. rewrite string_compare_Eq_correct. reflexivity.
Qed.

Lemma sender_is_not_owner : forall owner sender: address_constant,
    negb
      (comparison_to_int
         (address_compare
            owner sender) =? 0) %Z = true
    -> owner <> sender.
Proof.
  intros owner sender.
  rewrite Bool.negb_true_iff.
  rewrite Z.eqb_neq.
  unfold comparison_to_int.
  intuition.
  destruct H.
  rewrite H0. rewrite address_compare_Eq. reflexivity.
Qed.

Lemma sender_is_owner : forall owner sender: address_constant,
    negb
      (comparison_to_int
         (address_compare
            owner sender) =? 0) %Z = false
    -> owner = sender.
Proof.
  intros owner sender.
  rewrite Bool.negb_false_iff.
  rewrite (eqb_eq address).
  intuition.
Qed.

Lemma if_contract_correct :
  forall (input : data parameter_ty)
         (balances : data balances_ty)
         (owner : data owner_ty)
         (supply : data supply_ty)
         (ops : data (list operation))
         (storage_out : data storage_ty)
         (fuel : Datatypes.nat),
  let storage_in : data storage_ty := ((balances, owner), supply) in
  fuel >= 100 ->
  eval env if_contract fuel ((input, storage_in), tt) = Return ((ops, storage_out), tt)
  <->
  (sender env) = owner /\
  ((nil , storage_in , tt) = (ops , storage_out , tt )).
Proof.
  intros input balances owner supply ops storage_out fuel storage_in Hfuel.
  rewrite return_precond.
  unfold eval.
  rewrite eval_precond_correct.
  unfold ">=" in Hfuel.
  simpl.
  do 10 more_fuel ; simpl.
  destruct (negb (comparison_to_int (address_compare (sender env) owner) =? 0)%Z) eqn:condition.
  - do 2 more_fuel ; simpl. split.
    + (* -> *) intros H. contradiction H.
    + (* <- *) intros H.
      apply sender_is_not_owner in condition.
      intuition.
  - do 6 more_fuel ; simpl.
    split.
    + apply sender_is_owner in condition.
      intuition.
    + intuition.
Qed.
