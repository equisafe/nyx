type security_token_storage_t = {
  owner    : address ;
  balances : ( address , nat ) big_map ;
  tokens   : nat ;
}

type param_t = {
  amount : nat ;
  tr_to  : address ;
}

type security_token_ret_t = (operation list * security_token_storage_t)

let mint (param, s : param_t * security_token_storage_t)
  : security_token_ret_t =
  let check_sender =
    if (
      sender <> s.owner
    ) then failwith "error"
  in
  let new_balances =
    match Big_map.find_opt param.tr_to s.balances with
    | None -> Big_map.update param.tr_to (Some (param.amount)) s.balances
    | Some n -> Big_map.update param.tr_to (Some (n + param.amount)) s.balances
  in
  let new_s = {
    tokens = s.tokens + param.amount ;
    owner = s.owner ;
    balances = new_balances ;
  } in
  ( ([] : operation list) , new_s )
