Require Import Michocoq.macros.
Import syntax.
Require Import Michocoq.semantics.
Require Import Michocoq.util.
Import error.


Definition parameter_ty := unit.
Definition storage_ty := nat.

Module ST : (SelfType with Definition self_type := parameter_ty).
  Definition self_type := parameter_ty.
End ST.

Module helloworld(C:ContractContext)(E:Env ST C).

Module semantics := Semantics ST C E. Import semantics.

Definition helloworld : full_contract _ ST.self_type storage_ty :=
  ( CDR ;;
    NIL operation ;;
    PAIR
  ).

Theorem helloworld_correct :
  forall (input : data parameter_ty) storage_in
         (ops : data (list operation)) storage_out
         (fuel : Datatypes.nat),
  fuel >= 10 ->
  eval env helloworld fuel ((input, storage_in), tt) = Return ((ops, storage_out), tt)
  <->
  storage_in = storage_out /\
   ops = nil.
Proof.
  intros input storage_in ops storage_out fuel Hfuel.
  rewrite return_precond.
  unfold eval.
  rewrite eval_precond_correct.
  unfold ">=" in Hfuel.
  simpl.
  more_fuel ; simpl.
  more_fuel ; simpl.
  more_fuel ; simpl.
  intuition ; congruence.
Qed.

End helloworld.
