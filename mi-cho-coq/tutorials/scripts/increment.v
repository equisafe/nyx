Require Import Michocoq.macros.
Import Michocoq.syntax.
Import comparable.
Require Import NArith.
Require Import Michocoq.semantics.
Require Import Michocoq.util.
Import error.
Require Import ZArith.

Definition parameter_ty := unit.
Definition storage_ty := nat.

Module ST : (SelfType with Definition self_type := parameter_ty).
  Definition self_type := parameter_ty.
End ST.

Module increment(C:ContractContext)(E:Env ST C).

Module semantics := Semantics ST C E. Import semantics.

Definition increment : full_contract _ ST.self_type storage_ty :=
  ( CDR ;;
    PUSH nat (nat_constant 1) ;;
    ADD ;;
    NIL operation ;;
    PAIR
 ).

Lemma increment_correct :
  forall (input : data unit) storage_in
         (ops : data (list operation)) storage_out
         (fuel : Datatypes.nat),
  fuel >= 10 ->
  eval env increment fuel ((input, storage_in), tt) = Return ((ops, storage_out), tt)
  <->
  ((nil , N.add storage_in 1 , tt) = (ops , storage_out , tt )) /\
  ops = nil.
Proof.
  intros input storage_in ops storage_out fuel Hfuel.
  rewrite return_precond.
  unfold eval.
  rewrite eval_precond_correct.
  unfold ">=" in Hfuel.
  simpl.
  do 4 more_fuel ; simpl.
  destruct storage_in.
  - more_fuel.
    simpl. split.
    * intros. rewrite -> H. split.
      + reflexivity.
      + congruence.
    * intros. apply H.
  - more_fuel ; simpl. destruct p.
    * simpl. split.
      + intros. rewrite -> H. split.
        -- reflexivity.
        -- congruence.
      + intros. apply H.
    * simpl. split.
      + intros. rewrite -> H. split.
        -- reflexivity.
        -- congruence.
      + intros. apply H.
    * simpl. split.
      + intros. rewrite -> H. split.
        -- reflexivity.
        -- congruence.
      + intros. apply H.
Qed.

End increment.
