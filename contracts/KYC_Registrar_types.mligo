(**** storage types ****)

type investor_info_t = {
  country    : country_t ;
  region     : region_t ;
  rating     : rating_t ;
  expires    : epoch_time_t ;
  restricted : restricted_t ;
}
type kyc_t = ( investor_t , investor_info_t ) big_map

type registrar_storage_t = {
  owner : owner_t ;
  members : kyc_t ;
}

(**** I/O types ****)

type rating_restriction_t  = (rating_t  , limit_t) map
type country_restriction_t = {
  country_invest_limit : limit_t ;
  min_rating   : rating_t ;
  rating_restrictions : rating_restriction_t ;
}
type country_restrictions_t = (country_t, country_restriction_t) big_map

type add_members_t = ( investor_t * investor_info_t ) list
type remove_members_t = investor_t list
type get_member_t = investor_t * (investor_info_t -> operation list)
type check_member_el_t = {
  investor : investor_t ;
  was_balance_zero : bool ;
  is_balance_zero : bool ;
}
type check_member_t = (check_member_el_t list) * country_restrictions_t
type set_member_country_arg_t = investor_t * country_t
type registrar_ep_t =
  | AddMembers of add_members_t
  | RemoveMembers of remove_members_t
  | GetMember of get_member_t
  | CheckMember of check_member_t
  | SetMemberCountry of set_member_country_arg_t

type registrar_ret_t = (operation list * registrar_storage_t)
