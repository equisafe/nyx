#include "base_types.mligo"
#include "helpers.mligo"
#include "Event_Sink_types.mligo"


let eventsink_errors = {
  only_owner_can_set_perm = "51" ;
  not_allowed_to_post = "52" ;
}

let set_permission (p, s: set_permission_arg_t * eventsink_storage_t) =
  let check_owner = require (Tezos.sender = s.owner) eventsink_errors.only_owner_can_set_perm in
  let new_permissions =
    if p.1 then
      Set.add p.0 s.permissions
    else
      if Set.mem p.0 s.permissions then
        Set.remove p.0 s.permissions
      else
        s.permissions
  in
  let new_state = {
    owner = s.owner ;
    permissions = new_permissions ;
    messages = s.messages ;
    message_counter = s.message_counter ;
  } in
  ( ( [] : operation list) , new_state )

let post_event (p, s: post_event_arg_t * eventsink_storage_t) =
  let check_perm = require (Set.mem Tezos.sender s.permissions) eventsink_errors.not_allowed_to_post in
  let new_counter = s.message_counter + 1n in
  let new_messages = Big_map.update new_counter (Some {contract = Tezos.sender ; message = p }) s.messages in
  let new_state = {
    owner = s.owner ;
    permissions = s.permissions ;
    messages = new_messages ;
    message_counter = new_counter ;
  } in
  ( ( [] : operation list) , new_state )

let flush_messages (p, s: flush_messages_arg_t * eventsink_storage_t) =
  let check_owner = require (Tezos.sender = s.owner) eventsink_errors.only_owner_can_set_perm in
  let new_s = {
    owner = s.owner ;
    permissions = s.permissions ;
    messages = (Big_map.literal [] : messages_t) ;
    message_counter = 0n ;
  } in
  ( ( [] : operation list) , new_s )


let main (param, s: eventsink_ep_t * eventsink_storage_t) =
  match param with
  | PostEvent p -> post_event ( p , s )
  | SetPermission p -> set_permission ( p , s )
  | FlushMessages p -> flush_messages ( () , s )
