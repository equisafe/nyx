type fiat_rate_t = nat  (* fiat^-1 *)

type currency_t = address
type currency_info_t = {
  fiat_rate  : fiat_rate_t ;
  tokens_max : amount_t ;
  tokens     : amount_t ;
}
type currencies_t = ( currency_t , currency_info_t ) map

type crowdsale_info_t = {
  start       : epoch_time_t ;
  finish      : epoch_time_t ;
  completed   : epoch_time_t ;
  activated   : bool ;
  token       : address ;
  bonus_pct   : nat list ;
  bonus_times : nat list ;
}

type crowdsale_storage_t = {
  owner               : address ;
  receiver            : address ;
  currencies          : currencies_t ;
  fiat_max            : limit_t ;
  fiat                : amount_t ;
  crowdsale_info      : crowdsale_info_t ;
}

type manual_sale_t = address * amount_t * fiat_rate_t
type set_tz_fiat_rate_t = fiat_rate_t
type is_open_t = unit
type can_participate_t = address

type curr_to = currency_t
type curr_from = currency_t
type payable_t = curr_from * curr_to * amount_t
type set_currency_t = currency_t * fiat_rate_t * amount_t * amount_t

type crowdsale_ep_t =
  | IsOpen of is_open_t
  | CanParticipate of can_participate_t
  | Payable of payable_t
  | SetCurrency of set_currency_t

type crowdsale_ret_t = (operation list * crowdsale_storage_t)
