#include "base_types.mligo"
#include "helpers.mligo"
#include "Issuing_Entity_types.mligo"
#include "Security_Tokens_types.mligo"

let token_errors__not_owner = "31"                      // "Only owner or issuer can call this method."
let token_errors__balance_not_sufficient = "32"         // "Emitter balance in not sufficient."
let token_errors__not_enough_tokens_to_burn = "33"      // "not enough tokens to burn"
let token_errors__not_enough_tokens_to_mint = "34"      // "not enough tokens to mint"
let token_errors__transfer_from_not_allowed = "35"      // "You are not allowed to allow transfer from for this investor"
let token_errors__transfer_from_call_not_allowed = "36" // "You are not allowed to call transfer from."
let token_errors__transfer_from_amount_error = "37"     // "You cannot transfer this amount of tokens on behalf of this investor."
let token_errors__prev_amount_diff = "38"               // "Previous allowance amount is different than the provided one."


let make_modify_supply_op (was_balance_zero, is_balance_zero, investor, issuer: bool * bool * address * address) : operation list =
  let contract : modify_token_total_supply_arg_t contract = Operation.get_entrypoint "%modifyTokenTotalSupply" issuer in
  let modify_supply_info = {
    was_balance_zero = was_balance_zero ;
    is_balance_zero = is_balance_zero ;
    investor = investor ;
  } in
  [Operation.transaction modify_supply_info 0mutez contract]

let make_transfer_tokens_op (transfer_tokens_arg, issuer: transfer_tokens_arg_t * address) =
  let contract : transfer_tokens_arg_t contract = Operation.get_entrypoint "%transferTokens" issuer in
  Operation.transaction transfer_tokens_arg 0mutez contract [@@inline]


let make_transfer (transfer, balances : transfer_from_el_t * investor_amount_t)
    : investor_amount_t =
  let recipient_bal =
    match Big_map.find_opt transfer.tr_to balances with
    | None -> 0n
    | Some n -> n
  in
  let emitter_bal   = Big_map.find transfer.tr_from balances in
  if (emitter_bal < transfer.amount) then
    let throw = failwith token_errors__balance_not_sufficient in
    balances
  else
    let new_emitter_bal   = abs (emitter_bal - transfer.amount) in
    let new_recipient_bal = recipient_bal + transfer.amount in
    let new_balances =
      Big_map.update transfer.tr_to (Some new_recipient_bal)
        (Big_map.update transfer.tr_from (Some new_emitter_bal) balances)
    in
    new_balances

let transfer_helper (p, s: transfer_from_el_t * security_token_storage_t) =
  let new_balances = make_transfer (p , s.balances) in
  let zero_balances: zero_balances_t = {
    sender = match Big_map.find_opt p.tr_from new_balances with
      | Some n -> (n = 0n)
      | None -> true ;
    receiver = match Big_map.find_opt p.tr_to s.balances with
      | Some n -> (n = 0n)
      | None -> true ;
  } in
  let transfer_tokens_arg = {
    tr_from = p.tr_from ;
    tr_to = p.tr_to ;
    zero_balances = zero_balances ;
  } in
  let transfer_tokens_op = make_transfer_tokens_op ( transfer_tokens_arg , s.issuer ) in
  let new_s =
    {
      name = s.name ;
      allow_transfer_from = s.allow_transfer_from ;
      issuer = s.issuer ;
      tokens = s.tokens ;
      agreement_procedure = s.agreement_procedure ;
      balances = new_balances ;
      symbol = s.symbol ;
      owner = s.owner ;
      total_supply = s.total_supply ;
    }
  in
  ( transfer_tokens_op , new_s )

let check_permitted (s: security_token_storage_t): unit =
  if (
    sender <> s.issuer
    && sender <> s.owner
  ) then failwith token_errors__not_owner else unit [@@inline]

let set_allow_transfer_from_ (p, s: set_allow_transfer_from__t * security_token_storage_t) =
  let new_allow_transfer_from =
    if p.remove then
      Big_map.remove (p.transfer_from_info.transfer_for , p.authority) s.allow_transfer_from
    else
      Big_map.update (p.transfer_from_info.transfer_for , p.authority) (Some p.transfer_from_info.amount) s.allow_transfer_from
  in
  let new_s = {
    name = s.name ;
    allow_transfer_from = new_allow_transfer_from ;
    issuer = s.issuer ;
    tokens = s.tokens ;
    agreement_procedure = s.agreement_procedure ;
    balances = s.balances ;
    symbol = s.symbol ;
    owner = s.owner ;
    total_supply = s.total_supply ;
  } in
  ( ([] : operation list) , new_s )

let set_allow_transfer_from (p, s: set_allow_transfer_from_t * security_token_storage_t) =
  let check_prev_amount = match Big_map.find_opt (p.transfer_from_info.transfer_for , p.authority) s.allow_transfer_from with
    | Some prev_amount -> require (prev_amount = p.prev_amount) token_errors__prev_amount_diff
    | None -> ()
  in
  let is_allowed =
    if (
      sender <> s.issuer
      && sender <> s.owner
      && sender <> p.transfer_from_info.transfer_for
    ) then failwith token_errors__transfer_from_not_allowed else unit
  in
  let info = {
    authority = p.authority ;
    transfer_from_info = p.transfer_from_info ;
    remove = p.remove ;
  }
  in
  set_allow_transfer_from_ ( info , s )

let transfer (ps, s : transfer_t * security_token_storage_t)
    : security_token_ret_t =
  let aux = fun (acc, p: security_token_ret_t * transfer_el_t) ->
    let ops, current_s = acc in
    let op, new_s = transfer_helper ( { tr_from = Tezos.sender ; tr_to = p.tr_to ; amount = p.amount ; } , current_s ) in
    (op::ops , new_s)
  in
  List.fold aux ps (([] : operation list) , s)


let transfer_from (ps, s : transfer_from_t * security_token_storage_t)
  : security_token_ret_t =
  (* check that Tezos.sender is allowed to transfer p.amount for p.tr_from *)
  let aux = fun (acc, p: security_token_ret_t * transfer_from_el_t) ->
    let ops , current_s = acc in
    let allowed_amount =
      match Big_map.find_opt (p.tr_from , Tezos.sender) s.allow_transfer_from with
      | Some allowed_amount -> allowed_amount
      | None -> (failwith token_errors__transfer_from_call_not_allowed : amount_t)
    in
    let check2 = require (allowed_amount >= p.amount) token_errors__transfer_from_amount_error in
    (* Subtract amount form transfer_from allowance *)
    let new_transfer_from_info = {
      transfer_for = p.tr_from ;
      amount = abs (allowed_amount - p.amount) ;
    } in
    let empty_ops, tmp_s = set_allow_transfer_from_ ( { authority = Tezos.sender ; transfer_from_info = new_transfer_from_info ; remove = false ; } , current_s ) in
    let op, new_s = transfer_helper ( { tr_from = p.tr_from ; tr_to = p.tr_to ; amount = p.amount ; } , tmp_s ) in
    (op::ops , new_s)
  in
  List.fold aux ps (([] : operation list) , s)

let burn (p, s: burn_t * security_token_storage_t)
    : security_token_ret_t =
  let check_sender = check_permitted s in
  let new_balances =
    let recipient_bal = Big_map.find p.tr_to s.balances in
    let burnt = (* abs (recipient_bal - p.amount) in *)
      if (p.amount > recipient_bal) then
        let throw = failwith token_errors__not_enough_tokens_to_burn in
        recipient_bal
      else
        abs (recipient_bal - p.amount) in
    Big_map.update p.tr_to (Some burnt) s.balances
  in
  let was_balance_zero = match Big_map.find_opt p.tr_to s.balances with
    | None -> true
    | Some n -> n = 0n
  in
  let is_balance_zero = match Big_map.find_opt p.tr_to new_balances with
    | None -> true
    | Some n -> n = 0n
  in
  let is_balance_zero = true in
  let modify_supply_op = make_modify_supply_op ( was_balance_zero , is_balance_zero ,  p.tr_to , s.issuer ) in
  let new_s = {
    name = s.name ;
    allow_transfer_from = s.allow_transfer_from ;
    issuer = s.issuer ;
    tokens = s.tokens ;
    agreement_procedure = s.agreement_procedure ;
    symbol = s.symbol ;
    owner = s.owner ;
    balances = new_balances ;
    total_supply = relu (s.total_supply , p.amount) ;
  } in
  ( (modify_supply_op : operation list) , new_s )

let mint (p, s: mint_t * security_token_storage_t)
    : security_token_ret_t =
  let check_sender = check_permitted s in
  let new_tokens =
    if (p.amount > s.tokens) then
      let throw = failwith token_errors__not_enough_tokens_to_mint in
      s.tokens
    else
      abs (s.tokens - p.amount) in
  let new_balances =
    match Big_map.find_opt p.tr_to s.balances with
    | None -> Big_map.update p.tr_to (Some (p.amount)) s.balances
    | Some n -> Big_map.update p.tr_to (Some (n + p.amount)) s.balances
  in
  let was_balance_zero = match Big_map.find_opt p.tr_to s.balances with
    | None -> true
    | Some n -> n = 0n
  in
  let is_balance_zero = match Big_map.find_opt p.tr_to new_balances with
    | None -> true
    | Some n -> n = 0n
  in
  let modify_supply_op = make_modify_supply_op ( was_balance_zero , is_balance_zero ,  p.tr_to , s.issuer ) in
  let new_s = {
    name = s.name ;
    allow_transfer_from = s.allow_transfer_from ;
    issuer = s.issuer ;
    tokens = new_tokens ;
    agreement_procedure = s.agreement_procedure ;
    symbol = s.symbol ;
    owner = s.owner ;
    balances = new_balances ;
    total_supply = s.total_supply + p.amount ;
  } in
  ( (modify_supply_op : operation list) , new_s )

(* private method

The contract is designed with a storage that does not have an AuthorizedSupply
attribute. But we can recalculate it as follows
*)
let get_authorized_supply (s: security_token_storage_t)
    : amount_t =
  s.tokens + s.total_supply

(* private method *)
let balance_of (p: investor_t) (s: security_token_storage_t)
    : amount_t =
  Big_map.find p s.balances

let main (param, s : security_token_ep_t * security_token_storage_t)
    : security_token_ret_t =
  match param with
  | Transfer p -> transfer ( p , s )
  | Burn p -> burn (p , s)
  | Mint p -> mint (p , s)
  | SetAllowTransferFrom p -> set_allow_transfer_from ( p , s )
  | TransferFrom p -> transfer_from ( p , s )
