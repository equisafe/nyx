#include "base_types.mligo"
#include "helpers.mligo"
#include "Mini_Nyx_types.mligo"

let token_errors__not_owner = "31"                      // "Only owner or issuer can call this method."
let token_errors__balance_not_sufficient = "32"         // "Emitter balance in not sufficient."
let token_errors__not_enough_tokens_to_burn = "33"      // "not enough tokens to burn"
let token_errors__not_enough_tokens_to_mint = "34"      // "not enough tokens to mint"
let token_errors__transfer_from_not_allowed = "35"      // "You are not allowed to allow transfer from for this investor"
let token_errors__transfer_from_call_not_allowed = "36" // "You are not allowed to call transfer from."
let token_errors__transfer_from_amount_error = "37"     // "You cannot transfer this amount of tokens on behalf of this investor."
let token_errors__prev_amount_diff = "38"               // "Previous allowance amount is different than the provided one."

let make_transfer (transfer, balances : transfer_from_el_t * investor_amount_t)
  : investor_amount_t =
  let recipient_bal =
    match Big_map.find_opt transfer.tr_to balances with
    | None -> 0n
    | Some n -> n
  in
  let emitter_bal   = Big_map.find transfer.tr_from balances in
  if (emitter_bal < transfer.amount) then
    let throw = failwith token_errors__balance_not_sufficient in
    balances
  else
    let new_emitter_bal   = abs (emitter_bal - transfer.amount) in
    let new_recipient_bal = recipient_bal + transfer.amount in
    let new_balances =
      Big_map.update transfer.tr_to (Some new_recipient_bal)
        (Big_map.update transfer.tr_from (Some new_emitter_bal) balances)
    in
    new_balances

let transfer_helper (p, s: transfer_from_el_t * security_token_storage_t) =
  let new_balances = make_transfer (p , s.balances) in
  {
    name = s.name ;
    allow_transfer_from = s.allow_transfer_from ;
    tokens = s.tokens ;
    balances = new_balances ;
    symbol = s.symbol ;
    owner = s.owner ;
    total_supply = s.total_supply ;
  }

let check_permitted (s: security_token_storage_t): unit =
  if (
    sender <> s.owner
  ) then failwith token_errors__not_owner else unit [@@inline]

let set_allow_transfer_from_ (p, s: set_allow_transfer_from__t * security_token_storage_t) =
  let new_allow_transfer_from =
    if p.remove then
      Big_map.remove (p.transfer_from_info.transfer_for , p.authority) s.allow_transfer_from
    else
      Big_map.update (p.transfer_from_info.transfer_for , p.authority) (Some p.transfer_from_info.amount) s.allow_transfer_from
  in
  let new_s = {
    name = s.name ;
    allow_transfer_from = new_allow_transfer_from ;
    tokens = s.tokens ;
    balances = s.balances ;
    symbol = s.symbol ;
    owner = s.owner ;
    total_supply = s.total_supply ;
  } in
  ( ([] : operation list) , new_s )

let set_allow_transfer_from (p, s: set_allow_transfer_from_t * security_token_storage_t) =
  let check_prev_amount = match Big_map.find_opt (p.transfer_from_info.transfer_for , p.authority) s.allow_transfer_from with
    | Some prev_amount -> require (prev_amount = p.prev_amount) token_errors__prev_amount_diff
    | None -> ()
  in
  let is_allowed =
    if (
      sender <> s.owner
      && sender <> p.transfer_from_info.transfer_for
    ) then failwith token_errors__transfer_from_not_allowed else unit
  in
  let info = {
    authority = p.authority ;
    transfer_from_info = p.transfer_from_info ;
    remove = p.remove ;
  }
  in
  set_allow_transfer_from_ ( info , s )

let transfer (ps, s : transfer_t * security_token_storage_t)
  : security_token_ret_t =
  let aux = fun (current_s, p: security_token_storage_t * transfer_el_t) ->
    transfer_helper ( { tr_from = Tezos.sender ; tr_to = p.tr_to ; amount = p.amount ; } , current_s )
  in
  let new_s = List.fold aux ps s in
  (([] : operation list) , new_s)


let transfer_from_any_ = fun (current_s, p: security_token_storage_t * transfer_from_el_t) ->
  let allowed_amount =
    match Big_map.find_opt (p.tr_from , Tezos.sender) current_s.allow_transfer_from with
    | Some allowed_amount -> allowed_amount
    | None -> (failwith token_errors__transfer_from_call_not_allowed : amount_t)
  in
  let check2 = require (allowed_amount >= p.amount) token_errors__transfer_from_amount_error in
  (* Subtract amount form transfer_from allowance *)
  let new_transfer_from_info = {
    transfer_for = p.tr_from ;
    amount = abs (allowed_amount - p.amount) ;
  } in
  let empty_ops, tmp_s = set_allow_transfer_from_ ( { authority = Tezos.sender ; transfer_from_info = new_transfer_from_info ; remove = false ; } , current_s ) in
  transfer_helper ( { tr_from = p.tr_from ; tr_to = p.tr_to ; amount = p.amount ; } , tmp_s )

let transfer_from_admin_ = fun (current_s, p: security_token_storage_t * transfer_from_el_t) ->
  transfer_helper ( { tr_from = p.tr_from ; tr_to = p.tr_to ; amount = p.amount ; } , current_s )

let transfer_from (ps, s : transfer_from_t * security_token_storage_t) =
    let new_s = if Tezos.sender <> s.owner then
        List.fold transfer_from_any_ ps s
      else
        List.fold transfer_from_admin_ ps s
    in
    (([] : operation list) , new_s)


let burn (p, s: burn_t * security_token_storage_t)
  : security_token_ret_t =
  let check_sender = check_permitted s in
  let new_balances =
    let recipient_bal = Big_map.find p.tr_to s.balances in
    let burnt = (* abs (recipient_bal - p.amount) in *)
      if (p.amount > recipient_bal) then
        let throw = failwith token_errors__not_enough_tokens_to_burn in
        recipient_bal
      else
        abs (recipient_bal - p.amount) in
    Big_map.update p.tr_to (Some burnt) s.balances
  in
  let new_s = {
    name = s.name ;
    allow_transfer_from = s.allow_transfer_from ;
    tokens = s.tokens ;
    symbol = s.symbol ;
    owner = s.owner ;
    balances = new_balances ;
    total_supply = relu (s.total_supply , p.amount) ;
  } in
  ( ([] : operation list) , new_s )

let mint (p, s: mint_t * security_token_storage_t)
  : security_token_ret_t =
  let check_sender = check_permitted s in
  let new_tokens =
    if (p.amount > s.tokens) then
      let throw = failwith token_errors__not_enough_tokens_to_mint in
      s.tokens
    else
      abs (s.tokens - p.amount) in
  let new_balances =
    match Big_map.find_opt p.tr_to s.balances with
    | None -> Big_map.update p.tr_to (Some (p.amount)) s.balances
    | Some n -> Big_map.update p.tr_to (Some (n + p.amount)) s.balances
  in
  let new_s = {
    name = s.name ;
    allow_transfer_from = s.allow_transfer_from ;
    tokens = new_tokens ;
    symbol = s.symbol ;
    owner = s.owner ;
    balances = new_balances ;
    total_supply = s.total_supply + p.amount ;
  } in
  ( ([] : operation list) , new_s )

(* private method *)
let balance_of (p: investor_t) (s: security_token_storage_t)
  : amount_t =
  Big_map.find p s.balances

let main (param, s : security_token_ep_t * security_token_storage_t)
  : security_token_ret_t =
  match param with
  | Transfer p -> transfer ( p , s )
  | Burn p -> burn (p , s)
  | Mint p -> mint (p , s)
  | SetAllowTransferFrom p -> set_allow_transfer_from ( p , s )
  | TransferFrom p -> transfer_from ( p , s )
