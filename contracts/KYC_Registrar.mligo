#include "base_types.mligo"
#include "helpers.mligo"
#include "Multi_Sig_types.mligo"
#include "KYC_Registrar_types.mligo"

let kyc_errors__not_owner = "11"                    // "Only the owner of the contract modify KYC members"
let kyc_errors__investor_not_a_member = "12"        // "This investor is not a member."
let kyc_errors__country_restriction_failed = "13"   // "Country restriction failed."
let kyc_errors__investor_is_kyc_restricted = "14"   // "This investor is KYC restricted."

let check_sender (owner : owner_t) : unit =
  if sender = owner then unit else
    failwith kyc_errors__not_owner [@@inline]

let add_members (p, s: add_members_t * registrar_storage_t) : registrar_ret_t =
  let check1 = check_sender s.owner in
  let new_members =
    let aux = fun (prev, el: kyc_t * (investor_t * investor_info_t)) ->
      Big_map.update el.0 (Some el.1) prev in
    List.fold aux p s.members
  in
  let new_s =
    { owner = s.owner ;
      members = new_members ;
    } in
  ( ([] : operation list) , new_s )

let set_member_country (p, s: set_member_country_arg_t * registrar_storage_t) : registrar_ret_t =
  let check1 = check_sender s.owner in
  let inv = p.0 in
  let new_country = p.1 in
  let inv_info = Big_map.find inv s.members in
  let new_inv_info = {
    country = new_country ;
    region = inv_info.region ;
    rating = inv_info.rating ;
    expires = inv_info.expires ;
    restricted = inv_info.restricted ;
  } in
  let new_members = Big_map.update inv (Some new_inv_info) s.members in
  let new_s =
    { owner = s.owner ;
      members = new_members ;
    }
  in
  ( ([] : operation list) , new_s )

let remove_members (p, s: remove_members_t * registrar_storage_t) : registrar_ret_t =
  let check1 = check_sender s.owner in
  let new_members =
    let aux = fun (prev, el: kyc_t * investor_t) ->
      Big_map.remove el prev in
    List.fold aux p s.members
  in
  let new_s =
    { owner = s.owner ;
      members = new_members ;
    } in
  ( ([] : operation list) , new_s )

let get_member (p, s: get_member_t * registrar_storage_t) : registrar_ret_t =
  let inv = p.0 in
  let cb = p.1 in
  let inv_info = Big_map.find inv s.members in
  let ops = cb inv_info in
  ( ops , s )

type decrement_t = bool
type increment_t = bool
type check_member_cb_info_t = {
  country : country_t ;
  rating : rating_t ;
  decrement : decrement_t ;
  increment : increment_t ;
}
type update_country_counter_arg_t = check_member_cb_info_t list
let check_member_cb (args : update_country_counter_arg_t) =
  (* handle country and rating limits *)
  let contract = match (Tezos.get_entrypoint_opt "%updateCountryCounter" Tezos.sender : update_country_counter_arg_t contract option) with
    | Some n -> n
    | None -> (failwith "LOL" : update_country_counter_arg_t contract)
  in
  Operation.transaction args 0mutez contract

let check_member (p, s: check_member_t * registrar_storage_t) : registrar_ret_t =
  let invs = p.0 in
  let country_restrictions = p.1 in
  let args =
    let aux = fun (acc, check_member_el: update_country_counter_arg_t * check_member_el_t) ->
      let inv = check_member_el.investor in
      let inv_info =
        match Big_map.find_opt inv s.members with
        | Some n -> n
        | None -> (failwith kyc_errors__investor_not_a_member : investor_info_t)
      in
      let check1 =
        let check_restriction = require (not inv_info.restricted) kyc_errors__investor_is_kyc_restricted in
        let country_restr_exists_check = match Big_map.find_opt inv_info.country country_restrictions with
          | None -> failwith kyc_errors__country_restriction_failed
          | Some s -> ()
        in
        let country_info = Big_map.find inv_info.country country_restrictions in
        if country_info.min_rating <= inv_info.rating then
          ()
        else
          failwith kyc_errors__country_restriction_failed
      in
      let decrement =
        not check_member_el.was_balance_zero && check_member_el.is_balance_zero
      in
      let increment =
        check_member_el.was_balance_zero && not check_member_el.is_balance_zero
      in
      {
        decrement = decrement ;
        increment = increment ;
        country = inv_info.country ;
        rating = inv_info.rating ;
      }::acc
    in
    List.fold aux invs ([] : update_country_counter_arg_t)
  in
  let ops = ([check_member_cb args] : operation list) in
  ( ops , s)

let main (param, s: registrar_ep_t * registrar_storage_t) : registrar_ret_t =
  match param with
  | AddMembers p -> add_members (p , s)
  | RemoveMembers p -> remove_members (p , s)
  | GetMember p -> get_member (p , s)
  | CheckMember p -> check_member (p , s)
  | SetMemberCountry p -> set_member_country (p , s)
