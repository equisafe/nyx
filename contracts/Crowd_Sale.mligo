#include "base_types.mligo"
#include "helpers.mligo"
#include "Crowd_Sale_types.mligo"
#include "Security_Tokens_types.mligo"


let crowdsale_errors = {
    crowdsale_not_started = "41" ;                    // "Crowdsale has not started yet."
    crowdsale_finished = "42" ;                       // "Crowdsale is finished."
    crowdsale_completed = "43" ;                      // "Crowdsale is completed."
    owner_cannot_participate = "44" ;                 // "Owner cannot participate in crowdsale."
    must_pay_a_strictly_positive_amount = "45" ;      // "Must pay a positive amount to contract."
    max_tokens_already_sold = "46" ;                  // "The max amount of tokens has already been sold."
    max_fiat_already_reached = "47" ;                 // "The max amount of fiat has already been reached."
    cannot_exchange_between_same_currencies = "48" ;  // "Cannot exchange between same currencies."
}

(* Private methods *)

let check_excess_ (p: nat * nat * nat * epoch_time_t * (operation list)) =
  let sold = p.0 in
  let max = p.1 in
  let sent = p.2 in
  let crowdsale_completed = p.3 in
  let ops = p.4 in
  if (max = 0n) || (sold + sent < max) then
    ( ops , 0n, crowdsale_completed )
  else
    let excess = abs (sold + sent - max) in
    let refund_amount = Tezos.amount * excess in

    let source_contract =
      match (Tezos.get_contract_opt Tezos.source : unit contract option) with
      | Some contract -> contract
      | None -> (failwith "No contract" : unit contract)
    in
    let refund_op = Tezos.transaction unit refund_amount source_contract in
    let crowdsale_completed_ = Tezos.now in
    ( refund_op :: ops , excess , crowdsale_completed_ )

let payable (p, s: payable_t * crowdsale_storage_t) =
  let curr_from = p.0 in
  let curr_to = p.1 in
  let amount_ = p.2 in
  let crowdsale_checks =
    if s.crowdsale_info.activated then
      let check1 = require (Tezos.now >= s.crowdsale_info.start) crowdsale_errors.crowdsale_not_started in
      let check2 = require (Tezos.now < s.crowdsale_info.finish) crowdsale_errors.crowdsale_finished in
      let check3 = require (s.crowdsale_info.completed = (0 : timestamp)) crowdsale_errors.crowdsale_completed in
      // let check4 = require (Tezos.amount > 0tez) crowdsale_errors.must_pay_a_strictly_positive_amount in
      let check5 = require (Tezos.sender <> s.owner) crowdsale_errors.owner_cannot_participate in
      ()
    else
      ()
  in
  let is_to_tz =
    if curr_to = zero_address then true else false
  in
  let is_from_tz =
    if curr_from = zero_address then true else false
  in
  let check6 = require (curr_from <> curr_to) crowdsale_errors.cannot_exchange_between_same_currencies in
  let currency_info_from =
    match Map.find_opt curr_from s.currencies with
    | Some p -> p
    | None -> (failwith "Currency from does not exist" : currency_info_t)
  in
  let currency_info_to =
    match Map.find_opt curr_to s.currencies with
    | Some p -> p
    | None -> (failwith "Currency to does not exist" : currency_info_t)
  in

  (*
     We want to send a currA and get b currB in exchange. Both have a conversion
     rate to an underlying asset called "fiat". We therefore do:

         fiat = a * currA.fiat_rate  # dimension: currB * fiat / currB => fiat
         b = fiat / currB.fiat_rate  # dimension: fiat * currB / fiat  => currB

     We then send `a` currA to the receiver and `b` currB to Tezos.sender
  *)

  let fiat =
    if is_from_tz then
      Tezos.amount * currency_info_from.fiat_rate / 1tez
    else
      amount_ * currency_info_from.fiat_rate
  in
  let start_ops = ([] : operation list) in
  let fiat_check_excess_res = check_excess_ ( s.fiat , s.fiat_max , fiat , s.crowdsale_info.completed , start_ops ) in
  let fiat_refund_ops = fiat_check_excess_res.0 in
  let excess_fiat = fiat_check_excess_res.1 in
  let fiat_crowdsale_completed = fiat_check_excess_res.2 in
  let new_fiat = abs (fiat - excess_fiat) in
  let tokens = new_fiat / currency_info_to.fiat_rate in
  let token_check_excess_res = check_excess_ ( currency_info_to.tokens , currency_info_to.tokens_max , tokens , s.crowdsale_info.completed , fiat_refund_ops ) in
  let token_refund_ops = token_check_excess_res.0 in
  let excess_tokens = token_check_excess_res.1 in
  let token_crowdsale_completed = token_check_excess_res.2 in
  let new_tokens = tokens - excess_tokens in

  let transfer_fund_to_receiver_op =
    if is_from_tz then
      let receiver_contract =
        match (Tezos.get_contract_opt s.receiver : unit contract option) with
        | Some contract -> contract
        | None -> (failwith "No contract" : unit contract)
      in
      Tezos.transaction unit Tezos.balance receiver_contract
    else
      let contract : transfer_from_t contract = Operation.get_entrypoint "%transferFrom" curr_from in
      Operation.transaction { tr_to = s.receiver ; tr_from = Tezos.source ; amount = amount_ ; } 0mutez contract
  in
  let transfer_fund_to_receiver_ops = transfer_fund_to_receiver_op :: token_refund_ops in

  let contract : transfer_from_t contract = Operation.get_entrypoint "%transferFrom" curr_to in
  let transfer_tokens_op = Operation.transaction { tr_from = s.receiver ; tr_to = Tezos.source ; amount = tokens ; } 0mutez contract in
  let transfer_tokens_ops = transfer_tokens_op :: transfer_fund_to_receiver_ops in
  let new_fiat = s.fiat + fiat in
  let new_tokens = currency_info_from.tokens + tokens in
  let ops = transfer_tokens_ops in
  let new_s = {
    owner               = s.owner ;
    receiver            = s.receiver ;
    currencies          = s.currencies ;
    fiat_max            = s.fiat_max ;
    fiat                = new_fiat ;
    crowdsale_info      = s.crowdsale_info ;
  }
  in
  (ops , new_s)

let can_participate (p, s: can_participate_t * crowdsale_storage_t) =
  ( ([] : operation list) , s)

let is_open (p, s: is_open_t * crowdsale_storage_t) =
  ( ([] : operation list) , s)

let set_currency (p, s: set_currency_t * crowdsale_storage_t) =
  let currency = p.0 in
  let currency_info = {
    fiat_rate  = p.1 ;
    tokens_max = p.2 ;
    tokens     = p.3 ;
  }
  in
  let new_currencies = Map.update currency (Some currency_info) s.currencies in
  let new_s = {
    owner               = s.owner ;
    receiver            = s.receiver ;
    currencies          = new_currencies ;
    fiat_max            = s.fiat_max ;
    fiat                = s.fiat ;
    crowdsale_info      = s.crowdsale_info ;
  }
  in
  ( ([] : operation list) , new_s)

let main (param, s: crowdsale_ep_t * crowdsale_storage_t) : crowdsale_ret_t =
  match param with
  | IsOpen p -> is_open (p , s)
  | CanParticipate p -> can_participate (p , s)
  | Payable p -> payable (p , s)
  | SetCurrency p -> set_currency (p , s)
