type order_id_t = nat
type order_info_t = {
  sender : address ;
  token_from : token_t ;
  amount_from : amount_t ;
  token_to : token_t ;
  amount_to : amount_t ;
  deadline : timestamp ;
}
type dvp_storage_t = {
  owner : address ;
  order_book : (order_id_t , order_info_t * order_id_t) big_map ;
  counter : nat ;
}

type make_order_arg_t = order_info_t

type take_order_arg_t = {
  take_for: address;
  order_id: order_id_t;
}


type dvp_ep_t =
  | MakeOrder of make_order_arg_t
  | TakeOrder of take_order_arg_t

type dvp_ret_t = (operation list * dvp_storage_t)
