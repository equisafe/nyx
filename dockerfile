FROM debian:latest

RUN apt update
RUN apt-get -y install wget

RUN wget https://ligolang.org/deb/ligo.deb
RUN apt install ./ligo.deb

RUN apt-get -y install libsodium-dev libsecp256k1-dev libgmp-dev liblzma-dev curl pkg-config build-essential zlib1g-dev libncurses5-dev libgdbm-dev libnss3-dev libssl-dev libsqlite3-dev libreadline-dev libffi-dev curl libbz2-dev

# compile py3.8
WORKDIR /tmp
RUN curl -O https://www.python.org/ftp/python/3.8.2/Python-3.8.2.tar.xz
RUN tar -xf Python-3.8.2.tar.xz

WORKDIR /tmp/Python-3.8.2
RUN ./configure --enable-optimizations
RUN make -j 4
RUN make altinstall


# install nyx deps
WORKDIR /app
RUN cd /tmp && curl https://gitlab.com/equisafe/nyx/-/raw/master/requirements.txt > requirements.txt && python3.8 -m pip install --user -r requirements.txt

# configure environment:
#    - py.test is in .local
#    - CI is needed by pytest-tezos to hit sandbox
#      on tz rather than localhost
RUN echo "export PATH=$PATH:~/.local/bin" > ~/.bashrc
RUN echo "export CI=foo" >> ~/.bashrc
RUN echo "export PYTHONPATH=$PYTHONPATH:/nyx" >> ~/.bashrc

WORKDIR /nyx

entrypoint [""]
